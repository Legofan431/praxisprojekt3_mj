var PA3D = PA3D || {};
 
PA3D.Stream = function( data )
{
  this.data = data;
  this.offset = 0;
};

PA3D.Stream = function( data, offset )
{
  this.data   = data;
  this.offset = offset;
};

PA3D.Stream.prototype.TWO_POW_MINUS23 = Math.pow(2, -23);

PA3D.Stream.prototype.TWO_POW_MINUS126 = Math.pow(2, -126);

PA3D.Stream.prototype.readByte = function(){
  return this.data[this.offset ++] & 0xff;
};

PA3D.Stream.prototype.readInt32 = function(){
  var i = this.readByte();
  i |= this.readByte() << 8;
  i |= this.readByte() << 16;
  return i | (this.readByte() << 24);
};

PA3D.Stream.prototype.readFloat32 = function(){
  var m = this.readByte();
  m += this.readByte() << 8;

  var b1 = this.readByte();
  var b2 = this.readByte();

  m += (b1 & 0x7f) << 16; 
  var e = ( (b2 & 0x7f) << 1) | ( (b1 & 0x80) >>> 7);
  var s = b2 & 0x80? -1: 1;

  if (e === 255){
    return m !== 0? NaN: s * Infinity;
  }
  if (e > 0){
    return s * (1 + (m * this.TWO_POW_MINUS23) ) * Math.pow(2, e - 127);
  }
  if (m !== 0){
    return s * m * this.TWO_POW_MINUS126;
  }
  return s * 0;
};

PA3D.Stream.prototype.readString = function( len ){
  this.offset += len;

  return new TextDecoder( "utf-8" ).decode( this.data.subarray(this.offset - len, this.offset ) );
};

PA3D.Stream.prototype.readBinary = function( len ){
	  this.offset += len;

	  return this.data.subarray(this.offset - len, this.offset );
	};

PA3D.Stream.prototype.readArrayInt32 = function(array){
  var i = 0, len = array.length;
  
  while(i < len){
    array[i ++] = this.readInt32();
  }

  return array;
};

PA3D.Stream.prototype.readArrayFloat32 = function(array){
  var i = 0, len = array.length;

  while(i < len){
    array[i ++] = this.readFloat32();
  }

  return array;
};

PA3D.File = function( data, offset )
{
	try
	{
		var stream = new PA3D.Stream( data, offset );
	 
		this.magic     = stream.readString( 4 );
		this.fileSize  = stream.readInt32();
		this.dataBegin = stream.readInt32();	
		var headerStr  = stream.readString( this.dataBegin - 12 )		
		this.header    = JSON.parse( headerStr );
		
		var tmp = this.header.filename.substr( 0, this.header.filename.lastIndexOf( '.' ));
		this.header.cadnummer     = tmp.split( '@' )[ 0 ];
		this.header.configuration = tmp.split( '@' )[ 1 ];
		
		this.model     = this.header.cadnummer + "@" + this.header.configuration
		this.id        = this.model + "|" + this.header.filename;	
		this.dataSize  = this.fileSize - this.dataBegin;   
		this.data      = data;
		this.offset    = offset;
	}
	catch( err )
	{
		console.log( "ERROR: PA3D.File: " + err.message, headerStr );
	}
};

PA3D.Database = function()
{
	this.data      = [];
	this.files     = [];
	this.docs      = [];
	this.protos    = [];
	this.images    = {};
	this.instances = {}
	this.root      = new THREE.Object3D();
	this.root.name = "";
};

PA3D.Database.prototype.log = function( data )
{
	console.log( "PA3D.Database.log()" );

	for( var name in db.files ) 
	{		
		var file = db.files[ name ];
	
		console.log( file.id + ":" );
		console.log( file );
		
		if( file.header.format == 'json' )		
		{		
			var stream = new PA3D.Stream( file.data, file.offset + file.dataBegin );
			console.log( stream.readString( file.dataSize ));
		}
	}
}

PA3D.Database.prototype.traverse = function( parent, ref ) 
{													
	var doc = this.docs[ ref.name ] || this.docs[ ref.name.toLowerCase()]; 
	
	if( doc == null )
	{
		console.log( "PA3D.Database.traverse(): No document for reference ", ref.name );
		return;
	}

	if( doc.pmi.documenttype == "drawing" )
	{
		console.log( "PA3D.Database.traverse(): Drawings cannot be instantiated as 3d objects ", ref.name );
		return;
	}

	var name = parent.name + "|" + ref.name;
	var count = db.instances[ name ] || 0;

	db.instances[ name ] = ++count;

	name += ":" + count;	
		
	var overrideMat;	
	if( parent.userData.overrideMat )
		overrideMat = parent.userData.overrideMat;
	else if( ref.material )
		overrideMat = ref.material;			
	else if( doc.pmi.material )
		overrideMat = doc.pmi.material;	
	
	var instance;		
	if( doc.pmi.documenttype == "assembly" )
	{
		instance = new THREE.Object3D();
	}
	else if( doc.pmi.documenttype == "part" )
	{
		var proto = this.protos[ ref.name ] || this.protos[ ref.name.toLowerCase()];		
		if( proto == null )
		{
			console.log( "PA3D.Database.traverse(): No geometry for refName " + ref.name );
			return;
		}

		instance = proto.instantiate( overrideMat );				
	}
		
	if( ref.xf )
		instance.applyMatrix( ref.xf );
	
	instance.userData.positionOrig = instance.position.clone();
	instance.userData.pmi          = doc.pmi;
	instance.userData.doc          = doc;
	instance.userData.overrideMat  = overrideMat;
	instance.userData.attributes   = ref.attributes;
	instance.name                  = name;		
	
	parent.add( instance );
	
	for( var i = 0; i < doc.refs.length; i++ ) 
		this.traverse( instance, doc.refs[ i ]);					
}
						
PA3D.Database.prototype.init = function( data )
{
	pctmLoader = new PA3D.PCTMLoader( true );

	pctmLoader.colorPerVertex = true;
	
	this.data = new Uint8Array( data );
	
	this.rootdoc = null;
	
	var offset = 0;	
	while( offset < this.data.length )
	{
		var file = new PA3D.File( this.data, offset ); 
		if( file.magic != "cafs" )
			break;			
		
		var handle = ( file.header.filename.substring( 0, file.header.filename.lastIndexOf( "." ))).toLowerCase();
				
		if( file.header.filename == "metadata" )
		{
			var stream        = new PA3D.Stream( file.data, file.offset + file.dataBegin );
			var metadatastr   = stream.readString( file.dataSize - 1 );
			var metadata      = JSON.parse( metadatastr );			
			var filename      = metadata.rootfilename == null ? metadata.filename : metadata.rootfilename;
		    var configuration = metadata.rootconfiguration == null ? metadata.configuration : metadata.rootconfiguration; 
      			
			this.metadata               = metadata;
			this.metadata.filename      = filename;
			this.metadata.configuration = configuration;
			
			// 6.2 compatibility 
			
			delete metadata[ "rootfilename" ];
			delete metadata[ "rootconfiguration" ];
			delete metadata[ "username" ];
			
			if( this.rootdoc == null )				
				this.rootdoc = ( this.metadata.filename + "@" + this.metadata.configuration ).toLowerCase();	
		}		
		else if( file.header.type == "pmi" )
		{
			var stream = new PA3D.Stream( file.data, file.offset + file.dataBegin );
			var pmistr = stream.readString( file.dataSize - 1 );			
			var doc    = new PA3D.Document( pmistr );
			
			this.docs[ handle ] = doc;
		}		
		else if( file.header.format == "pctm" )
		{
			var proto  = pctmLoader.loadStream( file.data, file.offset + file.dataBegin, { useWorker: true } );
			proto.name = handle
			
			this.protos[ handle ] = proto;
		}
		else if( file.header.type == "product data" || file.header.type == "dbinfo" /* 6.2 Compatibility */ )
		{				
			this.drawings   = this.drawings || {};			
			var stream      = new PA3D.Stream( file.data, file.offset + file.dataBegin );
			var metadatastr = stream.readString( file.dataSize - 1 );							
			var data        = JSON.parse( metadatastr );			
			
			if( data.drawing )
				this.drawings[ handle ] = data;			
			else
			{
				// 6.2 Compatibility
				
				var at            = file.header.filename.lastIndexOf( "@" );
				var filename      = file.header.filename.substring( 0, at );
				var configuration = file.header.filename.substring( at + 1, file.header.filename.length - 3 );

				var part = { "part" : data.partnumber };
				
				var drawing = data;			
				
				drawing[ "drawing" ]       = data.drawingnumber;
				drawing[ "filename" ]      = filename;
				drawing[ "configuration" ] = configuration;
				drawing[ "description1" ]  = data[ "shortdesc1|D" ];
				drawing[ "description2" ]  = data[ "shortdesc2|D" ];
				drawing[ "description3" ]  = data[ "shortdesc3|D" ];
				drawing[ "description4" ]  = data[ "shortdesc4|D" ];		
				drawing[ "status" ]        = "";
				delete drawing[ "drawingnumber" ];
				delete drawing[ "partnumber" ];
				delete drawing[ "shortdesc1|D" ];
				delete drawing[ "shortdesc2|D" ];
				delete drawing[ "shortdesc3|D" ];
				delete drawing[ "shortdesc4|D" ];
				

				this.drawings[ handle ] = { "drawing": drawing, "part" : part };
			}
						
			
		}
		else if( file.header.type == "image" )
		{
			var stream = new PA3D.Stream( file.data, file.offset + file.dataBegin );
						
			var image = {}
			image.stream = stream.readBinary( file.dataSize - 1 );				
			image.format = file.header.format;;
			
			this.images[ handle ] = image;			
		}
		
		offset += file.fileSize;
	} 
	
	if( this.rootdoc == null )
		this.rootdoc = Object.keys( this.docs )[ 0 ];
	
	this.image = this.images[ this.rootdoc ];
				
	this.traverse( this.root, { "name": this.rootdoc });
};

PA3D.Database.prototype.loadUrl = function( url, callback ) 
{
    console.log( "PA3D.Database: Loading URL " + url );

	var scope = this;
	
	var xhr = new XMLHttpRequest();
	xhr.open( "GET", url + ((/\?/).test(url) ? "&" : "?") + (new Date()).getTime(), true );
	xhr.responseType = 'arraybuffer';
	
	xhr.onreadystatechange = function() 
	{
		if( xhr.readyState === 4 ) 
		{
			if ( xhr.status === 200 || xhr.status === 0 ) 
			{				
				scope.init( xhr.response );
				callback( scope.root );
			}
			else if( xhr.status == 404 )
			{
				document.getElementById( "loadingMsg" ).innerHTML = "Der Datenstream konnte nicht geladen werden!";
				document.getElementById( "loadingMsg" ).style.color = "red";		
				document.getElementById( "loadingMsg" ).style.fontSize = "14px";	
			}
		}
	}

	xhr.send( null );
};

PA3D.Document = function( pmistr ){
	try 
	{			
		this.pmi       = JSON.parse( pmistr );
		this.name      = this.pmi.filename + "@" + this.pmi.configurations[ 0 ].cfg_name;
		this.nameLower = this.name.toLowerCase()
		this.refs      = [];
						
		if( this.pmi.configurations[ 0 ].references )
		{
			for( var i = 0; i < this.pmi.configurations[ 0 ].references.length; i++ )
			{				
				var ref  = this.pmi.configurations[ 0 ].references[ i ];			
				var name = ( ref.ref_filename + "@" + ref.ref_configuration ).toLowerCase();
       				
				if( ref.instances == null )
				{
					console.log( "PA3D.Document: No instances for ", name ); 
				}
				else
				{
					for( var k = 0; k < ref.instances.length; k++ )
					{
						var inst = ref.instances[ k ];
						
						var suppressed = inst.inst_showbom == false || inst.show == false || inst.excluded || inst.hidden || inst.suppressed;
			            if( suppressed )
						{
							//console.log( "PA3D.Document: Suppressed " , name );
			            }
						else
						{				
							var xf = ref.instances[ k ].transform;									
							if( xf === undefined )
							{
								xf = [ 1, 0, 0, 0, 
								       0, 1, 0, 0, 
									   0, 0, 1, 0, 
									   0, 0, 0, 1 ];
							}
							else 
							{
								if( typeof( xf ) == "string" )
								{
									var s = xf.split( "," );
									xf = [ parseFloat( s[ 0 ]), parseFloat( s[ 1 ]), parseFloat( s[ 2 ]),  parseFloat( s[ 3 ]), 
										   parseFloat( s[ 4 ]), parseFloat( s[ 5 ]), parseFloat( s[ 6 ]),  parseFloat( s[ 7 ]), 
										   parseFloat( s[ 8 ]), parseFloat( s[ 9 ]), parseFloat( s[ 10 ]), parseFloat( s[ 11 ]), 
										   parseFloat( s[ 12 ]), parseFloat( s[ 13 ]), parseFloat( s[ 14 ]), parseFloat( s[ 15 ])];
								}
							}

							ref.instances[ k ].transform = xf;
							
							var	matrix = new THREE.Matrix4().set(
								xf[ 0 ], xf[ 4 ], xf[ 8 ],  xf[ 12 ], 
								xf[ 1 ], xf[ 5 ], xf[ 9 ],  xf[ 13 ], 
								xf[ 2 ], xf[ 6 ], xf[ 10 ], xf[ 14 ], 
								xf[ 3 ], xf[ 7 ], xf[ 11 ], xf[15] );
						
							
							// In case the instance provides additional transformations we combine them with the 
							// transformation information contained in the supplied transformation matrix
							
							if( inst.rotation || inst.scale || inst.translation )
							{
								var pos = new THREE.Vector3();
								var qtn = new THREE.Quaternion();
								var scl = new THREE.Vector3();
								
								matrix.decompose( pos, qtn, scl );
								
								if( inst.rotation )
									qtn.multiply( new THREE.Quaternion().setFromAxisAngle( 
										new THREE.Vector3( inst.rotation[ 0 ], inst.rotation[ 1 ], inst.rotation[ 2 ]), inst.rotation[ 3 ]));
								
								if( inst.scale )										
								{									
									scl.x *= inst.scale[ 0 ];
									scl.y *= inst.scale[ 1 ];
									scl.z *= inst.scale[ 2 ];
								}
								
								if( inst.translation )
								{
									pos.x += inst.translation[ 0 ];
									pos.y += inst.translation[ 1 ];
									pos.z += inst.translation[ 2 ];
								}
								
								matrix.compose( pos, qtn, scl );
							}								
																		
							var instance = { 
								"name": 	  name, 
								"xf": 		  matrix, 
								//"attributes": inst.attributes,
								"attributes": inst,
								"material":   inst.material, 
								"quantity":   ref.instances.length
							};
							
							this.refs.push( instance );							
			            }
					}
				}
			}
		}
		
		//console.log( "PA3D.Document: " + this.name );	
	}
	catch( err )
	{
		console.log( "PA3D.Document: " + err.message );
	}
};

PA3D.PartProto = function( geometry, material )
{
	this.geometry = geometry;
	this.material = material;
};

PA3D.PartProto.prototype.instantiate = function( overridemat )
{	
	var part = new THREE.Object3D();
		
	//console.log( "PA3D.PartProto: Instantiating ", part.name, overridemat );	
	
	for( var i = 0; i < this.geometry.length; i++ ) 
	{		
		var matRealistic;
		var matSimple;

		if( overridemat )
		{			
			var mat      = makeMaterial( overridemat, false );			
			matRealistic = mat.realistic;
			matSimple    = mat.simple;
		}
		else
		{
			matRealistic = this.material[ i ].realistic;	
			matSimple    = this.material[ i ].simple;				
		}

		var mesh = new THREE.Mesh( this.geometry[ i ], matSimple.clone());	

		mesh.userData.materials                = {};
		mesh.userData.materials[ 'realistic' ] = matRealistic.clone();
		mesh.userData.materials[ 'simple' ]    = matSimple.clone();	
		mesh.userData.materials[ 'basic' ]     = new THREE.MeshBasicMaterial({ color: 0xffffff, polygonOffset: true, polygonOffsetFactor: 0.2, polygonOffsetUnits: 1 }).clone();	
		mesh.userData.materialStyle            = 'simple';
		mesh.castShadow                        = mesh.material.opacity == 1;
		mesh.receiveShadow                     = mesh.material.opacity == 1;						
		
		part.add( mesh );
	}
		
	return part;
	
};

PA3D.PCTMLoader = function ( showStatus ) 
{
	THREE.Loader.call( this, showStatus );
};

PA3D.PCTMLoader.prototype = Object.create( THREE.Loader.prototype );


function makeMaterial( matJson, colorPerVertex )
{
	var dif = matJson.diffuseColor  ? matJson.diffuseColor  : [ 0.5, 0.5, 0.5 ];
	var spc = matJson.specularColor ? matJson.specularColor : [ 0.0, 0.0, 0.0 ];
	var ems = matJson.emissiveColor ? matJson.emissiveColor : [ 0.0, 0.0, 0.0 ];
	var shn = matJson.shininess     ? matJson.shininess     : 0.0;
	var tra = matJson.transparency  ? matJson.transparency  : 0.0;
	
	var gloss = ( spc[ 0 ] + spc[ 1 ] + spc[ 2 ]) / 3.0 + tra;
	
	var mat	= {};
	mat.realistic = new THREE.MeshPhysicalMaterial({
		emissive:           new THREE.Color( ems[ 0 ], ems[ 1 ], ems[ 2 ]),
		metalness:          gloss / 2.0,
		roughness:          1 - gloss,
		clearCoat:          gloss / 2,
		clearCoatRoughness: 1 - shn,
		reflectivity:       shn,
		opacity:            1 - tra,
		transparent:        tra > 0 ? true : false,			
		depthWrite:         tra > 0 ? false : true,			
		side:				tra > 0 ? THREE.DoubleSide : THREE.FrontSide
	});
					
	mat.simple = new THREE.MeshLambertMaterial({
		emissive:           new THREE.Color( ems[ 0 ], ems[ 1 ], ems[ 2 ]),
		opacity:            1 - tra,
		transparent:        tra > 0 ? true : false,			
		depthWrite:         tra > 0 ? false : true,			
		side:				tra > 0 ? THREE.DoubleSide : THREE.FrontSide
	});		
	
	/*
	mat.simple = new THREE.MeshPhongMaterial({
		emissive:           new THREE.Color( ems[ 0 ], ems[ 1 ], ems[ 2 ]),
		specular:           new THREE.Color( spc[ 0 ], spc[ 1 ], spc[ 2 ]),
		shininess:          shn * 100, // interval [ 0, 100 ]
		opacity:            1 - tra,
		transparent:        tra > 0 ? true : false,			
		depthWrite:         tra > 0 ? false : true,			
		side:				tra > 0 ? THREE.DoubleSide : THREE.FrontSide
	});		
	*/
	
	if( colorPerVertex )
	{
		mat.realistic.vertexColors = THREE.VertexColors;
		mat.simple.vertexColors    = THREE.VertexColors;
	}
	else
	{
		mat.realistic.vertexColors = THREE.NoColors;
		mat.simple.vertexColors    = THREE.NoColors;		
		mat.realistic.color        = new THREE.Color( dif[ 0 ], dif[ 1 ], dif[ 2 ]);
		mat.simple.color           = new THREE.Color( dif[ 0 ], dif[ 1 ], dif[ 2 ]);
	}
	
	return mat;
}

PA3D.PCTMLoader.prototype.loadStream = function( data, offset, parameters ) 
{
    var stream     = new PA3D.Stream( data, offset );
	var magic      = stream.readString( 4 );
	var dataBegin  = stream.readInt32();
	var header     = stream.readString( dataBegin - 8 );
	var jsonObject = JSON.parse( header );	

	var materials = [], geometries = [], numAttributes = [];

	var pos = dataBegin + offset;
		
	for( var i = 0; i < jsonObject.materials.length; i ++ ) 
	{
	    materials[ i ] = makeMaterial( jsonObject.materials[ i ], this.colorPerVertex );
				
		var str    = new CTM.Stream( stream.data );
		str.offset = pos;
	
		var ctmFile  = new CTM.File( str );
		var geometry = this.createModel( ctmFile );
					
		// compute vertex normals if not present 
		
		if( geometry.attributes[ "normal" ] === undefined ) 
		{
			//var geometry2 = new THREE.Geometry();
			//geometry2.fromBufferGeometry( geometry );		
			//geometry2.computeFlatVertexNormals();		
			//mat.realistic.flatSshading = true;
			//mat.simple.flatSshading  = true;			
			//geometry = geometry2;
			
			//mesh.userData.materials[ 'realistic' ] =			
			//console.log( "Computing vertex normals" );			
			//computeVertexNormals( geometry );					
			geometry.computeVertexNormals();
		}
				
		numAttributes.push( geometry.attributes.position.count );
		
		geometries.push( geometry );
					
		pos = pos + jsonObject.meshsizes[ i ];
	}
	
	if( this.colorPerVertex )
	{
		var geometryMerged;
		if( geometries.length == 1 )
			geometryMerged = geometries[ 0 ];
		else	
			geometryMerged = THREE.BufferGeometryUtils.mergeBufferGeometries( geometries );	
			
		var colors = new Uint8Array( geometryMerged.attributes.position.count * 3 );	
		
		var off = 0;
		for( var i = 0; i < jsonObject.materials.length; i++ ) 
		{
			var r = jsonObject.materials[ i ].diffuseColor[ 0 ];
			var g = jsonObject.materials[ i ].diffuseColor[ 1 ];
			var b = jsonObject.materials[ i ].diffuseColor[ 2 ];
								
			for( var j = 0; j < numAttributes[ i ]; j++ ) 
			{			
				colors[( off + j ) * 3 + 0 ] = r * 255;
				colors[( off + j ) * 3 + 1 ] = g * 255;
				colors[( off + j ) * 3 + 2 ] = b * 255;
			}
			
			off = off + numAttributes[ i ];
		}
		
		geometryMerged.addAttribute( 'color', new THREE.BufferAttribute( colors, 3, true ));
		
		geometries = [];
		geometries.push( geometryMerged );

		var mat = materials[ 0 ];
		
		materials = [];
		materials.push( mat );
	}
	
	return new PA3D.PartProto( geometries, materials );
}

function computeVertexNormals( geometry ) 
{
	var indices    = geometry.index !== null ? geometry.index.array : undefined;
	var attributes = geometry.attributes;
	var positions  = attributes.position.array;
	var normals    = attributes.normal !== undefined ? attributes.normal.array : undefined;
	var colors     = attributes.color  !== undefined ? attributes.color.array  : undefined;
	var uvs        = attributes.uv     !== undefined ? attributes.uv.array     : undefined;
	var uvs2       = attributes.uv2    !== undefined ? attributes.uv2.array    : undefined;

	var faceVertexUvs = [[]];
	
	if( uvs2 !== undefined ) 
		faceVertexUvs[ 1 ] = [];

	var tempNormals = [];
	var tempUVs     = [];
	var tempUVs2    = [];

	var vertices = [];
	var colors   = [];
	var faces    = [];
	var edges    = {};
	
	for( var i = 0, j = 0; i < positions.length; i += 3, j += 2 ) 
	{
		vertices.push( new THREE.Vector3( positions[ i ], positions[ i + 1 ], positions[ i + 2 ] ) );

		if( normals !== undefined ) 
			tempNormals.push( new THREE.Vector3( normals[ i ], normals[ i + 1 ], normals[ i + 2 ] ) );
		
		if( colors !== undefined ) 
			colors.push( new THREE.Color( colors[ i ], colors[ i + 1 ], colors[ i + 2 ] ) );

		if( uvs !== undefined ) 
			tempUVs.push( new THREE.Vector2( uvs[ j ], uvs[ j + 1 ] ) );

		if( uvs2 !== undefined ) 
			tempUVs2.push( new THREE.Vector2( uvs2[ j ], uvs2[ j + 1 ] ) );
	}

	var cb = new THREE.Vector3(), ab = new THREE.Vector3();
	
	function addFace( a, b, c, materialIndex, position ) 
	{
		var vertexNormals = normals !== undefined ? [ tempNormals[ a ].clone(), tempNormals[ b ].clone(), tempNormals[ c ].clone() ] : [];
		var vertexColors  = colors  !== undefined ? [ colors[ a ].clone(),      colors[ b ].clone(),      colors[ c ].clone() ] : [];

		var face = new THREE.Face3( a, b, c, vertexNormals, vertexColors, materialIndex );
	
		var vA = vertices[ face.a ];
		var vB = vertices[ face.b ];
		var vC = vertices[ face.c ];

		cb.subVectors( vC, vB );
		ab.subVectors( vA, vB );
		cb.cross( ab );
		cb.normalize();

		face.normal.copy( cb );
		face.position = position;
		
		faces.push( face );

		if( uvs !== undefined ) 
			faceVertexUvs[ 0 ].push( [ tempUVs[ a ].clone(), tempUVs[ b ].clone(), tempUVs[ c ].clone() ] );

		if( uvs2 !== undefined ) 
			faceVertexUvs[ 1 ].push( [ tempUVs2[ a ].clone(), tempUVs2[ b ].clone(), tempUVs2[ c ].clone() ] );
	}

	var groups = geometry.groups;

	if( groups.length > 0 ) 
	{
		for ( var i = 0; i < groups.length; i ++ ) 
		{
			var group = groups[ i ];
			var start = group.start;
			var count = group.count;

			for ( var j = start, jl = start + count; j < jl; j += 3 ) 
			{
				if( indices !== undefined )
					addFace( indices[ j ], indices[ j + 1 ], indices[ j + 2 ], group.materialIndex, j );
				else 
					addFace( j, j + 1, j + 2, group.materialIndex, j );
			}
		}
	} 
	else 
	{
		if( indices !== undefined ) 
			for ( var i = 0; i < indices.length; i += 3 ) 
				addFace( indices[ i ], indices[ i + 1 ], indices[ i + 2 ], undefined, i );
		else 
			for ( var i = 0; i < positions.length / 3; i += 3 ) 
				addFace( i, i + 1, i + 2, undefined, i );
	}
	
	var index1, index2, indexLo, indexHi;
	var key, keys = [ 'a', 'b', 'c' ];
	var edge;
	
	var vertexEdges   = [];
	var vertexFaces   = [];
	var vertexNormals = [];
	for( var i = 0; i < vertices.length; i++) 
	{			
		vertexEdges.push([]);
		vertexFaces.push([]);
		vertexNormals.push([]);
	}
	
	for( var i = 0, l = faces.length; i < l; i ++ ) 
	{
		var face = faces[ i ];
		
		vertexFaces[ face.a ].push( i );
		vertexFaces[ face.b ].push( i );
		vertexFaces[ face.c ].push( i );
		
		for( var j = 0; j < 3; j ++ ) 
		{
			index1 = face[ keys[ j ] ];
			index2 = face[ keys[ ( j + 1 ) % 3 ] ];
			indexLo = Math.min( index1, index2 );
			indexHi = Math.max( index1, index2 );

			key = indexLo + ',' + indexHi;
		
			edge = edges[ key ];
			
			if( edge === undefined ) 
				edges[ key ] = { index1: indexLo, index2: indexHi, face1: i, face2: undefined };
			else 
			{
				edge.face2 = i;				
				edge.angle = 180.0 / Math.PI * Math.acos( faces[ edge.face1 ].normal.dot( faces[ edge.face2 ].normal ));
												
				vertexEdges[ index1 ].push( edge );
				vertexEdges[ index2 ].push( edge );
			}		
		}
	}
	
	console.log( "vertices: ", vertices.length );
	console.log( "colors:   ", colors.length );
	console.log( "faces: ",    faces.length );
	
	var verticesNew = []; // { old, new, faces }
	
	
	var creaseAngle = 30;
		
	var current = vertices.length;
	
	for( i in vertexEdges )
	{
		var ve = vertexEdges[ i ];
		
		console.log( "vertex: ", i, " edges: ", ve.length );
		
		var indexCopy = { indexOld: i, faces: []};
		
		var edgeCurrent = 0;
		var count = 0;
		
		while( count++ < ve.length )
		{
			var edge = ve[ edgeCurrent ];
			
			console.log( "    edge: ", edgeCurrent, " vertices: ", edge.index1, edge.index2, " faces: ", edge.face1, edge.face2, " angle: ", edge.angle, " position: ", faces[ edge.face1 ].position );
			
			if( edge.angle >= creaseAngle )
			{
				console.log( "    duplicating vertex" );
				
				indexCopy.indexNew = current++;
				indexCopy.faces.push( edge.face2 );							
			}
				
			// Find adjoinig edge
			
			for( j = 0; ve.length; j++ )
			{
				if( j == edgeCurrent )
					continue;
				
				if( edge.face2 == ve[ j ].face1 || edge.face2 == ve[ j ].face2 )
				{
					edgeCurrent = j;
					break;
				}
			}			
		}
		
		if( indexCopy.faces.length )
			verticesNew.push( indexCopy );
	}
	
	console.log( "Number of duplicated vertices: ", verticesNew.length );
	
	var num = 0;
	for( i = 0; i < verticesNew.length; i++ )
	{
		console.log( verticesNew[ i ].indexOld, " -> ", verticesNew[ i ].indexNew, " faces: ", verticesNew[ i ].faces );		
		
		num += verticesNew[ i ].faces.length;
	}
	
	console.log( "Number of chaged face vertices: ", num );		

	//var geometryNew = new THREE.BufferGeometry();	
	//geometryNew.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );		
}
	
PA3D.PCTMLoader.prototype.createModel = function ( file ) {

	var Model = function() 
	{
		THREE.BufferGeometry.call( this );

		this.materials = [];

		// init GL buffers
		var vertexIndexArray = file.body.indices,
		vertexPositionArray = file.body.vertices,
		vertexNormalArray = file.body.normals;
		
		var vertexUvArray, vertexColorArray;

		if ( file.body.uvMaps !== undefined && file.body.uvMaps.length > 0 ) {
			vertexUvArray = file.body.uvMaps[ 0 ].uv;
		}

		if ( file.body.attrMaps !== undefined && file.body.attrMaps.length > 0 && file.body.attrMaps[ 0 ].name === "Color" ) {
			vertexColorArray = file.body.attrMaps[ 0 ].attr;
		}

		this.setIndex( new THREE.BufferAttribute( vertexIndexArray, 1 ) );
		this.addAttribute( 'position', new THREE.BufferAttribute( vertexPositionArray, 3 ) );

		if ( vertexNormalArray !== undefined ) 
			this.addAttribute( 'normal', new THREE.BufferAttribute( vertexNormalArray, 3 ) );

		if ( vertexUvArray !== undefined ) 
			this.addAttribute( 'uv', new THREE.BufferAttribute( vertexUvArray, 2 ) );

		if ( vertexColorArray !== undefined ) 
			this.addAttribute( 'color', new THREE.BufferAttribute( vertexColorArray, 4 ) );

	}

	Model.prototype = Object.create( THREE.BufferGeometry.prototype );

	return new Model();
};