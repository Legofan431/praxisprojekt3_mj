var transparentModeOpacity = 0.1;
var wireframeModeOpacity = 0.6;
var displayMode = "3d";
var tooltipEnabled = false;
var axeshelperEnabled = false;
var activeMode = "";
var prevSelected;
var nextToSelect;
var lastPreviewPlane;
var searchIndex = {};
var productinformation = {};
var translation = {};

//Speichert Ansichten
var view = {
    "drawingStyle": "shaded",
    "perspective": "initial",
}


var modeDisplay = document.createElement( "div" );
modeDisplay.setAttribute( "id", "modeDisplay" );
document.body.appendChild( modeDisplay );

var annotations = new Annotations();
var measurements = new Measurements();
var views = new Views();
var table = new Table();

//Load options
var storedOptions;
var request = new XMLHttpRequest();
request.open( "GET", "config.json",  true );
request.responseType = "json";
request.send();

request.onload = function(){
    if( typeof ( request.response ) == "string" )
        storedOptions = JSON.parse( request.response );
    storedOptions = storedOptions || request.response;
}

function xlt( text )
{
	var translatedText = translation[ text ];
	if( translatedText != null )
		return translatedText;
	
	return text;
}

function getUrlParams( url ) 
{
	var queryString = url ? url.split( '?' )[ 1 ] : window.location.search.slice( 1 );

	var obj = {};
	if( queryString ) 
	{
		queryString = queryString.split( '#' )[ 0 ];

		var arr = queryString.split( '&' );

		for( var i = 0; i < arr.length; i++ ) 
		{
			var a = arr[ i ].split( '=' );

			var paramNum = undefined;
			var paramName = a[ 0 ].replace(/\[\d*\]/, function( v ) 
			{
				paramNum = v.slice( 1 , -1 );
				return '';
			});

			var paramValue = typeof( a[ 1 ]) === 'undefined' ? true : a[ 1 ];

			paramName  = paramName.toLowerCase();
			paramValue = paramValue.toLowerCase();

			if( obj[ paramName ]) 
			{
				if( typeof obj[ paramName ] === 'string' ) 
					obj[ paramName ] = [ obj [ paramName ]];
				
				if( typeof paramNum === 'undefined' ) 
					obj[ paramName ].push( paramValue );
				else 
					obj[ paramName ][ paramNum ] = paramValue;
			}
			else 
				obj[ paramName ] = paramValue;
		}
	}

	return obj;
}

/*-------Toolbarfunktionen-----------*/
function refreshPage(){
    socket.close();
    location.reload( true );
  }

function setActiveMode( mode ){
    activeMode = mode;
    showModeDisplay( false );

    if( measurements.lastPreviewPlane != null ){
        annotations.remove( measurements.lastPreviewPlane );
        measurements.lastPreviewPlane = null;
    }
    measurements.showPreviewPlane = false;

    transformControls.detach();
    scene.remove( transformControls );

    document.getElementById( "toolbarBtnObjHide" ).style.backgroundColor = "transparent";
    document.getElementById( "toolbarBtnSetCenter" ).style.backgroundColor = "transparent";
    document.getElementById( "toolbarMeasureBtn" ).style.backgroundColor = mode.indexOf( "measure" ) == 0 ? "orange" : "transparent";
    document.getElementById( "toolbarBtnMovemode" ).style.backgroundColor = "transparent";


    switch( mode ){
        case "objHideMode":
            document.getElementById( "toolbarBtnObjHide" ).style.backgroundColor = "orange";
            hideSelected( false );
            showModeDisplay( true, "Modus aktiv: Komponenten ausblenden" );
            break;
        case "setRotationCenterMode":
            document.getElementById( "toolbarBtnSetCenter" ).style.backgroundColor = "orange";
            showModeDisplay( true, "Modus aktiv: Rotationszentrum festlegen" );
            break;
        case "measureThickness":
            showModeDisplay( true, "Materialstärke messen: Bitte Punkt wählen" );
            break;
        case "measurePointToPoint":
            showModeDisplay( true, "Punkt-zu-Punkt-Distanz messen: Bitte ersten Punkt wählen" );
            break;
        case "measurePlaneToPoint":
            showModeDisplay( true, "Ebene-zu-Punkt-Distanz messen: Bitte Ebene wählen" );
            measurements.showPreviewPlane = true;
            break;
        case "measureAngle":
            showModeDisplay( true, "Winkel messen: Bitte erste Fläche wählen");
            measurements.showPreviewPlane = true;
            break;
        case "move":
            document.getElementById( "toolbarBtnMovemode" ).style.backgroundColor = "orange";
            if( nextToSelect ){
                selectObject( nextToSelect, false );
                enableMovementControlsForObj( nextToSelect );
            }
            showModeDisplay( true, "Komponenten verschieben" );
            break;
    }
}

function toggleDisplayMode()
{
    displayMode = displayMode == "2d" ? "3d" : "2d";
    setDisplayMode( displayMode )
}

function setDisplayMode( mode )
{
	displayMode = mode;
    var btnElem = document.getElementById( "toolbarBtnDisplaymode" );
    
    if( mode == "2d" )
	{
        document.getElementById( "canvas" ).style.display = "none";
        document.getElementById( "canvas2d" ).style.display = "block";     

        if( btnElem ){
            btnElem.title     = "Zur 3D-Ansicht wechseln";
            btnElem.innerHTML = "3D";	
        }	

        enableCanvasCtrl( false );
    }
	else if( mode == "3d" )
	{
        document.getElementById( "canvas" ).style.display = "block";
        document.getElementById( "canvas2d" ).style.display = "none";  

        if( btnElem ){
            btnElem.title     = "Zur 2D-Ansicht wechseln";
            btnElem.innerHTML = "2D";	
        }
        
        enableCanvasCtrl( true );
    }	
}

function applyHighLightMaterial( obj )
{		
	if( obj.type != "Mesh" )
		return;
	
	if( obj.userData.isHighlighted )
	{
		var mat = obj.material;		
		mat.transparent = false;
        mat.depthWrite  = true;
		mat.opacity     = 1;
		
		if( mat.type != "MeshBasicMaterial"  )
		{
			mat.color    = new THREE.Color().setRGB( 0.5, 0.0, 0.0 );				
			mat.emissive = new THREE.Color().setRGB( 0.75, 0.0, 0.0 );
		}
		else
		{
			mat.color = new THREE.Color().setRGB( 1.0, 0.0, 0.0 );				
		}							
	}	
	else
	{
		var matOrig = obj.userData.materials[ obj.userData.materialStyle ];
		var mat     = obj.material;		
		
		mat.transparent = matOrig.transparent;
		mat.opacity     = matOrig.opacity;
		mat.color       = matOrig.color;
        mat.emissive    = matOrig.emissive;

        if( view.drawingStyle == "transparent" || view.drawingStyle == "transparentEdges" )
            setTransparency( obj );
        if( view.drawingStyle == "wireframe" )
            setTransparency( obj, wireframeModeOpacity );
	}
}

function setMaterialStyle( style )
{
    rootObj3d.traverse( function( obj )
	{
        if( obj.type == "Mesh" && obj.name != "BoundingBox" )
		{
			obj.userData.materialStyle = style;			
            obj.material = obj.userData.materials[ style ].clone();	
			
			applyHighLightMaterial( obj );			
		}
    });		
}

function setDrawingStyle( style )
{
    view.drawingStyle = style;
	
	controls.maxPolarAngle = Math.PI;	
	
    switch( style )
	{
        case "shaded":
		    setMaterialStyle( 'simple' );
            showEdges( false );
			showOutline( false );
			showStyleRealistic( false );
            renderer.context.enable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

            document.getElementById( "canvas" ).style.backgroundColor = "";

 			headlight.visible = true;
	
            break;
        case "shadedEdges":
		    setMaterialStyle( 'simple' );
            showEdges( true );
			showOutline( false );
			showStyleRealistic( false );
            renderer.context.enable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

            document.getElementById( "canvas" ).style.backgroundColor = "";

 			headlight.visible = true;
	
            break;
        case "realistic":
		    setMaterialStyle( 'realistic' );
            showEdges( false );
            showOutline( false );
            showStyleRealistic( true );

            renderer.context.enable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
            renderer.shadowMap.enabled = true;	

			headlight.visible = false;

            document.getElementById( "canvas" ).style.backgroundColor = "";
			
			controls.maxPolarAngle = Math.PI/2; 
				
            break;
        case "transparent":
            setMaterialStyle( 'simple' );
            showEdges( false );
            showOutline( false );			
            showStyleRealistic( false );
            setTransparency( rootObj3d );

            renderer.context.disable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

 			headlight.visible = true;
			
            document.getElementById( "canvas" ).style.backgroundColor = "";
            break;
        case "transparentEdges":
            setMaterialStyle( 'simple' );
            showEdges( true );
            showOutline( false );			
            showStyleRealistic( false );
            setTransparency( rootObj3d );

            renderer.context.disable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

 			headlight.visible = true;
			
            document.getElementById( "canvas" ).style.backgroundColor = "";
            break;
        case "wireframe":
            setMaterialStyle( 'basic' );
            showEdges( true );
            showOutline( true );
            showStyleRealistic( false );
            setTransparency( rootObj3d, wireframeModeOpacity );

            renderer.context.enable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

 			headlight.visible = true;
			            
			document.getElementById( "canvas" ).style.backgroundColor = "white";	

            break;
        case "documentation":
            setMaterialStyle( 'basic' );
            showStyleRealistic( false );
						
            document.body.style.cursor = "wait"; //TODO: Der Cursor wird erst anders dargestellt, sobald die Maus bewegt wird
			
			showEdges( true );
			showOutline( true );
			
			document.getElementById( "canvas" ).style.backgroundColor = "white";
	
            renderer.context.enable( renderer.context.DEPTH_TEST );
            renderer.sortObjects       = true;
			renderer.shadowMap.enabled = false;	

 			headlight.visible = false;
			
            document.body.style.cursor = "default";
            break;
    }	
}

function setTransparency( obj3d, opacity )
{    
    obj3d.traverse( function( obj )
	{
        if( obj.type == "Mesh" && !obj.userData.isHighlighted )
		{
            obj.material.transparent = true;	   				
            obj.material.opacity     = opacity || transparentModeOpacity;			
			obj.material.side        = THREE.DoubleSide;
        }
    });
}

function setPolygonOffset( obj3d )
{
    obj3d.traverse( function( obj )
	{
        if( obj.type == "Mesh" )
		{
            obj.material = obj.material.clone();
            obj.material.polygonOffset       = true;
            obj.material.polygonOffsetFactor = 0.2;
            obj.material.polygonOffsetUnits  = 1;
        }
    });	
}

function showEdges( show, forceReload )
{
    let edgesToDelete = [];

    let splashScreen = document.getElementById( "loadingMsg" );
   
   

    rootObj3d.traverse( function( obj )
	{
        if( obj.type == "Mesh" )
		{
            if( show )
			{ 
                splashScreen.style.fontSize = "";
                splashScreen.innerHTML = "Kanten werden berechnet";
                splashScreen.style.display = "block";
                enableCanvasCtrl( false );

                if( forceReload && obj.edge )
                    obj.parent.remove( obj.edge );

                var edge;
                if( obj.edge == undefined || forceReload )
				{ 
                    var angleThreshold = document.getElementById( "drawingModeThresholdSlider" ).value || 35;
                    var edges          = new THREE.EdgesGeometry( obj.geometry.clone(), angleThreshold );
					var lineMat        = new THREE.LineBasicMaterial({ color: 0x000000 });
                    edge               = new THREE.LineSegments( edges, lineMat );
                }
				else
                    edge = obj.edge;
                
                obj.parent.add( edge );
                obj.edge = edge;	
                
                splashScreen.style.display = "none";
                enableCanvasCtrl( true );
            }
            else
                edgesToDelete.push( obj );
        }
    });   
    for( i in edgesToDelete ){
        let obj = edgesToDelete[i];
        obj.parent.remove( obj.edge );
    } 
}

function showOutline( value )
{
    if( value )
        effect = new THREE.OutlineEffect( renderer, { defaultThickness: 0.002 });
    else
        effect = null;	
}

function showStyleRealistic( show )
{    
	var group = scene.getObjectByName( "realisticObjects" );
    scene.remove( group );
    
    if( show ){
        group      = new THREE.Object3D();
        group.name = "realisticObjects";

        var bbox     = new THREE.Box3().setFromObject( rootObj3d );
        var bbsize   = new THREE.Vector3();				
        var bbcenter = new THREE.Vector3();
        
        bbox.getSize( bbsize );					
        bbox.getCenter( bbcenter );

        var objSize   = bbsize.length();					
        var objCenter = bbcenter;

        var lightTargetObj = new THREE.Object3D();		
        lightTargetObj.position.set( objCenter.x, objCenter.y, objCenter.z );
        group.add( lightTargetObj );

        var lights = [];
		
        var light1 = new THREE.DirectionalLight( 0xffffff, 0.5 );
        light1.position.set( objCenter.x + objSize, objCenter.y + ( objSize * 2 ), objCenter.z + objSize * 1.5 );
        lights.push( light1 );

        var light2 = new THREE.DirectionalLight( 0xffffff, 0.5 );
        light2.position.set( objCenter.x - objSize * 1.5, objCenter.y + ( objSize * 2 ), objCenter.z - objSize );
        lights.push( light2 );

        for( i in lights )
        {
            var light = lights[ i ];
            light.castShadow            = true;
            light.shadow.camera.right   = objSize;
            light.shadow.camera.left    = - objSize;
            light.shadow.camera.top     = objSize;
            light.shadow.camera.bottom  = - objSize;
            light.shadow.camera.near    = camera.near;
            light.shadow.camera.far     = objSize * 4;
            light.shadow.mapSize.width  = 4096;
            light.shadow.mapSize.height = 4096;
            light.target                = lightTargetObj;
            
            //group.add( new THREE.CameraHelper( light.shadow.camera ));
            group.add( light );
        }
                        
        var hemisphereLight = new THREE.HemisphereLight( 0xa3ddff, groundColor, 0.1 );
        group.add( hemisphereLight );
                                                    
        var ground           = new THREE.Mesh( new THREE.PlaneBufferGeometry( 25, 25 ), groundMat );
        ground.receiveShadow = true;
        ground.rotation.x    = -Math.PI/2;
        group.add( ground );		
        
        group.position.set( objCenter.x, bbox.min.y - 0.0001, objCenter.z );
        
        scene.add( group );
    }
}

function toggleAxeshelper(){
    var btn = document.getElementById( "toolbarBtnAxeshelper" );
    if( axeshelperEnabled ){
        scene.remove( axesHelper );
        btn.title = "Koordinatensystem anzeigen";
        btn.style.backgroundColor = "";
    }else{
        scene.add( axesHelper );
        btn.title = "Koordinatensystem ausblenden";
        btn.style.backgroundColor = "orange";
    }

    axeshelperEnabled = !axeshelperEnabled;
}

function showModeDisplay( show, content ){
    if( show ){       
        modeDisplay.innerHTML = content;
        modeDisplay.style.display = "block";

        modeDisplay.style.transition = "color 0s";
        modeDisplay.style.color = "#ff66ff";

        modeDisplay.style.transition = "";
        modeDisplay.style.color = "";
    }else{
        modeDisplay.style.display = "none";
    }
}

function toggleObjHideMode(){    
    setActiveMode( activeMode == "objHideMode" ? "" : "objHideMode" );
}

function toggleSetCenterMode(){
    setActiveMode( activeMode == "setRotationCenterMode" ? "" : "setRotationCenterMode" );
}

function toggleTooltipDisplay(){
    tooltipEnabled = !tooltipEnabled;
    
    document.getElementById( "toolbarBtnToggleTooltip" ).title = tooltipEnabled ? "Tooltip deaktivieren" : "Tooltip aktivieren";
    document.getElementById( "toolbarImgToggleTooltip" ).src = !tooltipEnabled ? "img/icon_tooltipDisable.png" : "img/icon_tooltipEnable.png";
    
    if( tooltipEnabled ){
        table.showObjInfo( nextToSelect );
    }else
        table.hideInfoContainer();
}

function popupWindow( selector ){
    enableCanvasCtrl( false );

    var content = "";

    switch( selector ){
        case "adjustProductStructure":
            content = "<h4>Produktstruktur konfigurieren</h4>";

            let options = [ "Position", "Strukturtiefe", "Anzahl", "Zustand", "Zeichnung", "Indexnummer", "CAD-Nummer", 
                "Konfiguration", "Teil", "Bezeichnung", "Bezeichnung2", "Hinweis" ];
            for( i in options )
                content += "<input type='checkbox' value='" + options[i] + "'" + (storedOptions.productStructure[options[i]] ? " checked" : "") + ">" + options[i] + "<br>\n";     
            break;


        case "adjustCoordinateSystem":
            content = "<h4>Koordinatensystem festlegen</h4>";
            content += "<p>Welche Achse soll nach oben zeigen?</p>";
            for( var i = 0; i < 3; i++ ){
                var axis;
                switch( i ){
                    case 0: axis = "X"; break;
                    case 1: axis = "Y"; break;
                    case 2: axis = "Z"; break;
                }
                content += "<input type='radio' name='axis' value=" + axis + ( storedOptions.rootObjectRotation.axis == axis ? " checked" : "" ) + ">" + axis + "-Achse<br/>"
            }
            content += "<br/>";
            content += "<input type='checkbox' id='cbInvert' value='invertAxis'" + ( storedOptions.rootObjectRotation.inverted ? " checked" : "" ) + ">Achse invertieren<br/>";
            break;
        

        case "viewOrder":
            content += "<h4>Reihenfolge der Ansichten</h4>";
            if( views.list.length == 0 ){
                content += "<p>Keine Ansichten hinzugefügt.</p>";
            }else{
                content += "<p>(Verschieben mit Drag & Drop)</p>";
            }
            content += "<ol class='draggable'>";
            for( i in views.list ){
                content += '<li class="draggableItem" draggable="true" ondragover="dragOver(event)" ondragstart="dragStart(event)">' + views.list[i].name + '</li>';
            }
            content += "</ol>";
            break;
        

        case "animationDuration":
            content += "<h4>Animationsdauer festlegen</h4>";
            content += "<p>Angaben in Millisekunden</p>";
            content += "Montage\t<input class=animationSettingsInput type=number value=" + storedOptions.animation.assembly + "><br/>"
            content += "Sonstige\t<input class=animationSettingsInput type=number value=" + storedOptions.animation.duration + ">";
            break;
    }

    document.getElementById( "popupWindowSave" ).onclick = function(){popupWindowSave( selector )};
    document.getElementById( "popupWindowCancel" ).onclick = function(){popupWindowCancel()};

    document.getElementById( "popupWindowContent" ).innerHTML = content;
    document.getElementById( "popupWindow" ).style.display = "block";
}

function popupWindowSave( selector ){
    let content = document.getElementById( "popupWindowContent" );
    switch( selector ){
        case "adjustProductStructure":
            for( i in content.children )
                    storedOptions.productStructure[content.children[i].value] = content.children[i].checked;

            if( document.getElementById( "objInfoContainer" ).style.display != "none" )
                table.showProductStructure();
            break;


        case "adjustCoordinateSystem":
            for( i in content.childNodes ){
                if( content.children[i].checked ){
                    storedOptions.rootObjectRotation.axis = content.children[i].value;
                    break;
                }
            }
            storedOptions.rootObjectRotation.inverted = document.getElementById( "cbInvert" ).checked;

            rotateRootObject();
            var group = scene.getObjectByName( "realisticObjects" );
            if( group != null )
                scene.remove( group );
            if( view.drawingStyle == "realistic" )
                showStyleRealistic( true );
            break;

        
        case "viewOrder":
            var items = content.children[2].children; //0: title 1: description 2: list
            var order = [];
            for( var i = 0; i < items.length; i++ ){
                order.push( items[i].innerHTML );
            }
            views.setOrder( order );
            break;


        case "animationDuration":
            storedOptions.animation.assembly = content.children[2].value;
            storedOptions.animation.duration = content.children[4].value;
            break;
    }
    document.getElementById( "popupWindow" ).style.display = "none";
    enableCanvasCtrl( true );
    //TODO: Save the object "storedOptions" into the file config.json
}

function popupWindowCancel(){
    document.getElementById( "popupWindow" ).style.display = "none";
    enableCanvasCtrl( true );
}

/*Taken from the internet*/
var _el;

function dragOver(e) {
  e.preventDefault();
  if (isBefore(_el, e.target))
    e.target.parentNode.insertBefore(_el, e.target);
  else
    e.target.parentNode.insertBefore(_el, e.target.nextSibling);
}

function dragStart(e) {
  e.dataTransfer.effectAllowed = "move";
  e.dataTransfer.setData("text/plain", null); //Thanks to bqlou for their comment.
  _el = e.target;
}

function isBefore(el1, el2) {
  if (el2.parentNode === el1.parentNode)
    for (var cur = el1.previousSibling; cur && cur.nodeType !== 9; cur = cur.previousSibling)
      if (cur === el2)
        return true;
  return false;
}
/*----------------------------------*/

function hideObject( obj3d, hide )
{
    if( obj3d == null )
		return;
    
        obj3d.traverse( function( obj ){
            if( obj.type == "Object3D" )
                obj.visible = !hide;
                //Checkbox an- oder abwählen
                let cb = document.getElementById( "psCb" + table.nameIdToGeneric( obj.name ) );
                if( cb ){
                    cb.checked = !hide;
                }
        } );
}

function hideSelected( isolate )
{
    if ( nextToSelect ){
        selectObject( nextToSelect, false );           
       
        if( isolate ){
            rootObj3d.traverse( function( obj ){
                if( obj != rootObj3d ){
                    hideObject( obj, true );
                }
            } );
            nextToSelect.traverseAncestors( function( obj ){
                obj.visible = true;
            } );
            hideObject( nextToSelect, false );

            let bbox = new THREE.Box3().setFromObject( nextToSelect );		
            let bbcenter = new THREE.Vector3();	
            let bbsize   = new THREE.Vector3();		
            bbox.getCenter( bbcenter );		
            bbox.getSize( bbsize );

            let size = bbsize.length();	

            setCamPos( new THREE.Vector3().addVectors( bbcenter, new THREE.Vector3( size * 2, size * 2, size * 2 ) ), 
                        new THREE.Vector3( 0, 1, 0 ), bbcenter );
        }else{
             hideObject( nextToSelect, true );
        }
        nextToSelect = null;
        prevSelected = null;
    }
}

function prepareModel( model )
{
	model.traverse( function( obj )
	{
        if( obj.type == "Object3D" ){	
			obj.userData.isHighlighted = false;			
		}
	});
}

function setRotationCenter( point ){
    if( point ){
        new TWEEN.Tween( controls.target ).to( {
                x: point.x,
                y: point.y,
                z: point.z
            }, 500 )
            .easing( TWEEN.Easing.Quadratic.InOut )
            .start();
        
        return point;
    }
    return 0;
}

function resetRotationCenter(){
    setRotationCenter( objCenter );
}

function setCamPos( vPos, camUp, target, preventAnimation ){
    var duration = preventAnimation ? 0 : storedOptions.animation.duration;
    var easing = TWEEN.Easing.Quadratic.InOut;
    var onEnd = function(){
        camera.position.set( vPos.x, vPos.y, vPos.z );
        camera.up = camUp;
        controls.target = target.clone();
    }
    new TWEEN.Tween( camera.position ).to( {
            x: vPos.x, 
            y: vPos.y, 
            z: vPos.z 
        }, duration )
        .easing( easing )
        .onComplete( onEnd )
        .start();

    new TWEEN.Tween( camera.up ).to( {
            x: camUp.x,
            y: camUp.y,
            z: camUp.z
        }, duration )
        .easing( easing )
        .onComplete( onEnd )
        .start();

    new TWEEN.Tween( controls.target ).to( {
            x: target.x,
            y: target.y,
            z: target.z
        }, duration )
        .easing( easing )
        .onUpdate( function(){
            camera.lookAt( this );
        })
        .onComplete( onEnd )
        .start();
    
    requestRender();
}

function setPerspective( key, preventAnimation ){
    var perspectivesToSave = [ "top", "bottom", "left", "right", "front", "back", "initial" ];
    for( i in perspectivesToSave ){
        if( key == perspectivesToSave[i] ){
            view.perspective = key;
        }
    }

    var bbox = new THREE.Box3().setFromObject( rootObj3d );
    var objCenter = new THREE.Vector3();
    bbox.getCenter( objCenter );

    switch( key ){
        case "initial":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( objSize * 2, objSize * 2, objSize * 2 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "top":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( 0, objSize * 2, objSize / 1000 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "bottom":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( 0, - objSize * 2, objSize / 1000 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "left":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( - objSize * 2, 0, 0 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "right":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( objSize * 2, 0, 0 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "front":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( 0, 0, objSize * 2 ) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "back":
            setCamPos( new THREE.Vector3().addVectors( objCenter, new THREE.Vector3( 0, 0, - objSize * 2) ),
                        new THREE.Vector3( 0, 1, 0 ),
                        objCenter,
                        preventAnimation );
            break;
        case "cameraPerspective":
            if( camera == cameraPerspective )
                break;
            cameraPerspective.position.set( cameraOrthographic.position.x, cameraOrthographic.position.y, cameraOrthographic.position.z );
            camera = cameraPerspective;   
            
            onWindowResize(); //Update the aspect ratio

            controls.object = camera;
            controls.update();
            break;
        case "cameraOrthographic":
            if( camera == cameraOrthographic )
                break;
            cameraOrthographic.position.set( cameraPerspective.position.x, cameraPerspective.position.y, cameraPerspective.position.z );
            camera = cameraOrthographic;

            onWindowResize(); //Update the aspect ratio

            controls.object = camera;
            controls.update();
            break;
    }
}

function explodeProduct( obj, level, complete ) 
{		
	if( !obj.userData.pmi || obj.userData.pmi.documenttype != "assembly" )
		return;

	level++;
	
	var descend = true;
	if( !obj.userData.exploded )
	{
		explodeAssy( obj );
	    obj.userData.exploded = obj.type == "Object3D";
		descend = false;
	}
		
	if( descend || complete )
		for( var i = 0; i < obj.children.length; i++ ) 
		{
			explodeProduct( obj.children[ i ], level, complete );
		}
}

function explodeComplete()
{		
	explodeProduct( rootObj3d, 0, true );
}

function explodeLevel()
{		
	explodeProduct( rootObj3d, 0, false );
}
		
function explodeAssy( assy )
{	
	var bbox     = new THREE.Box3().setFromObject( assy );
	var bbsize   = new THREE.Vector3();				
	var bbcenter = new THREE.Vector3();
						
	bbox.getSize( bbsize );					
	bbox.getCenter( bbcenter );
					
	var size = bbsize.length();
	
	for( i in assy.children )
	{         
		var obj = assy.children[ i ];
		
		var bboxObj     = new THREE.Box3().setFromObject( obj );
		var bbsizeObj   = new THREE.Vector3();				
		var bbcenterObj = new THREE.Vector3();
					
		bboxObj.getSize( bbsizeObj );					
		bboxObj.getCenter( bbcenterObj );
						
		
		var objPosWorld = new THREE.Vector3();
		obj.getWorldPosition( objPosWorld );		
			
		var traWorld = bbcenterObj.clone();
		traWorld.sub( bbcenter );
		
		var objPosNewWorld = objPosWorld.clone();
		objPosNewWorld.add( traWorld );

		if( assy.parent.name == "" && view.drawingStyle == "realistic" )
			objPosNewWorld.y = objPosNewWorld.y + bbsize.y * 0.5;
	
		var objPosNewLocal = assy.worldToLocal( objPosNewWorld );

		new TWEEN.Tween( obj.position )
		.to( {
			x: objPosNewLocal.x,
			y: objPosNewLocal.y,
			z: objPosNewLocal.z
		}, 1000 )
		.easing( TWEEN.Easing.Quadratic.InOut )  
		.start();			
	}
}
	
function restoreObjectPositions()
{
    rootObj3d.traverse( function( obj )
    {
        if( obj.type == "Object3D" && !obj.userData.positionOrig.equals(obj.position) ){
            new TWEEN.Tween( obj.position )
            .to({
                x: obj.userData.positionOrig.x,
                y: obj.userData.positionOrig.y,
                z: obj.userData.positionOrig.z
            }, storedOptions.animation.duration )
            .easing( TWEEN.Easing.Quadratic.InOut )
            .start();
        }
        
         obj.userData.exploded = false; 
	});	
}

function enableMovementControlsForObj( obj3d ){
    transformControls.detach();
    if( obj3d )
    {
        transformControls.attach( obj3d ); 
        scene.add( transformControls );
    }
    else{
        scene.remove( transformControls );  
    }
}

/*--------------Selektion, Deselektion und Highlighting------------------*/
function getMouseCoords( event ){
    var mouse = [];
    var bRect = renderer.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX - bRect.left ) / ( bRect.width - bRect.left ) ) *2 -1;
    mouse.y = - ( ( event.clientY - bRect.top ) / ( bRect.bottom- bRect.top ) ) *2 +1;
    return mouse;
}

function onMouseDown( event ){
    var mouse = getMouseCoords( event );
    mouseDownX = mouse.x;
    mouseDownY = mouse.y;	
    
    showContextMenu( false );	
    hideSubmenus();

    document.getElementById( "viewNamer" ).blur();
    document.getElementById( "searchField" ).blur();
}

function onMouseUp( event )
{
    var mouse = getMouseCoords( event );
    
    if( Math.abs( mouse.x - mouseDownX ) < 0.01 && Math.abs( mouse.y - mouseDownY ) < 0.01 )
	{
        if( event.button == 2 )
            showContextMenu( true );
		else
		{        
            if( activeMode == "objHideMode" )
				
                hideObject( pickObject( mouse ), true );
				
			else if( activeMode == "setRotationCenterMode" ){
				
                if( setRotationCenter( pickPoint( mouse ) ) ){
                    setActiveMode("");
                }
				
            }else if( activeMode.indexOf( "measure" ) == 0 )
				
                measurements.measure( mouse );
				
			else if( activeMode == "move" ){
                enableMovementControlsForObj( pickObject( mouse ) );
            } else{
                selectObjectByMouseCoordinates( mouse );   
            }
        }  
    }
}

function onMouseMove( event ){
    var mouse = getMouseCoords( event );
    if( measurements.lastPreviewPlane != null ){
        annotations.remove( measurements.lastPreviewPlane );
        measurements.lastPreviewPlane = null;
    }
    if( measurements.showPreviewPlane ){
        var obj = pickObject( mouse );
        if( obj != null ){
            var plane = measurements.placePlaneOnObject( obj, pickPoint( mouse ), pickFace( mouse ) );
            measurements.lastPreviewPlane = plane.planeHelper;
        }
    }
}

function selectObjectByMouseCoordinates( mouse )
{
    var pickedObj = pickObject( mouse );
        
    if( pickedObj == prevSelected && pickedObj != null ){
        nextToSelect = nextToSelect.parent.name == "" ? nextToSelect : nextToSelect.parent;
    }else{
        nextToSelect = pickedObj;
    }
    prevSelected = pickedObj;

    selectNone();
    let trs = document.getElementById( "objInfo" ).children[1];
    if( trs ){
        trs = trs.children;
        for( i in trs ){
            table.highlightTableRow( trs[i].id, false );
        }
    }
    
    if( pickedObj == null ){
        prevSelected = null;
        nextToSelect = null;
    }else{

        table.highlightTableRow( table.nameIdToGeneric( nextToSelect.name ), true );
        table.highlightTableRow( table.nameIdToGeneric( nextToSelect.name.substring( nextToSelect.name.lastIndexOf( "|" )) ), true );
        selectObject( nextToSelect, true );

        table.showObjInfo( nextToSelect );
    }
	
	var message;
	if( nextToSelect )
	{
		var instName = nextToSelect.name.substring( nextToSelect.name.lastIndexOf( "|" ) + 1 );
		var pos      = instName.lastIndexOf( ":" );
		var objName  = pos >= 0 ? instName.substring( 0, pos ) : instName;
		var drawing  = db.drawings[ objName ].drawing;
		var part     = db.drawings[ objName ].part;
		
		message = "select|" + drawing.objectid;

		if( part )
			message += "|" + part.part;

		// + "|" + confNumber;		
	}
	else
	{
		message = "selectnothing";
    }
    
    document.title = message;
}

function selectNone()
{
	selectObject( rootObj3d, false );
}

function selectAll()
{
	selectObject( rootObj3d, true );
}

function selectInvert()
{
	rootObj3d.traverse( function( obj )
	{
		obj.userData.isHighlighted = !obj.userData.isHighlighted;
        applyHighLightMaterial( obj );
    });
}
	
function selectObject( obj3d, value )
{
    if( value ) nextToSelect = obj3d;
    obj3d.traverse( function( obj )
	{		
		obj.userData.isHighlighted = value;
		applyHighLightMaterial( obj );
    }); 
    table.showObjInfo( obj3d );
}

function pickObject( mouse ){
    raycaster.setFromCamera( mouse, camera );

    var intersects = raycaster.intersectObjects( rootObj3d.children, true );
    var selectedObj;

    if( intersects.length > 0 ){
        for( i in intersects ){
            if( intersects[i].object.type == "Mesh" && intersects[i].object.name != "ground" ){
                selectedObj = intersects[i].object.parent;
                break;
            }
        }
    }

    return selectedObj;
}

function pickPoint( mouse ){
    raycaster.setFromCamera( mouse, camera );

    var pickedPoint;
    var intersects = raycaster.intersectObjects( rootObj3d.children, true );
    if( intersects.length > 0 ){
        for( i in intersects ){
            if( intersects[i].object.type == "Mesh" ){
                pickedPoint = intersects[i].point;
                break;
            }
        }
    }
    return pickedPoint;
}

function pickFace( mouse ){
    raycaster.setFromCamera( mouse, camera );

    var pickedFace;
    var intersects = raycaster.intersectObjects( rootObj3d.children, true );
    if( intersects.length > 0 ){
        for( i in intersects ){
            if( intersects[i].object.type == "Mesh" ){
                pickedFace = intersects[i].face;
                break;
            }
        }
    }
    return pickedFace;
}

function handleSearchKeypress( event ){
    if( event.key == "Enter" ){
        search();
    }  
}

function search(){
    let textField = document.getElementById( "searchField" );
    let searchStr = textField.value.toLowerCase();
    
    textField.blur();

    if( Object.keys(searchIndex).length == 0){
        rootObj3d.traverse( function( obj ){
            if( obj.type == "Object3D" ){
                let name = table.nameIdToGeneric(obj.name.substring( obj.name.lastIndexOf("|") +1 ) );
                if( searchIndex[name] == null ){
                    let data = table.getObjData( obj );
                    for( header in data ){
                        for( prop in data[header] ){
                            data[header][prop] = data[header][prop].toString().toLowerCase();
                        }
                    }
                    searchIndex[name] = data;
                }
            }
        } )
    }

    let tableStr = table.header( ["Dokument", "Merkmal", "Position" ] );

    let matches = [];
    let pos = 0;
    for( docName in searchIndex ){
        let data = searchIndex[docName];
        let isAdded = false;

        for( i in matches){
            if( matches[i].docName == docName ){
                isAdded = true;
                break;
            }
        }
        if( isAdded )
            continue;

        let isDocAdded = false;
        for( header in data ){
            if( isDocAdded ) break;
            for( property in data[header] ){

                if( data[header][property].indexOf( searchStr ) != -1 ){
                    tableStr += table.tableRow( [docName, data[header][property], header], "id = \"|" + 
                        docName + "\" onclick='table.trOnMouseClick(" + pos + ", true)' pos=" + pos );
                    
                    matches.push( docName );
                    pos++;
                    isDocAdded = true;
                    break;
                }
            }
        }
    }

    table.showInfoContainer( tableStr );

    selectNone();

    rootObj3d.traverse( function( obj ){
        let objName = table.nameIdToGeneric( obj.name );
        objName = objName.substring( objName.lastIndexOf("|") +1 );

        for( i in matches ){
            if( matches[i] == objName ){
                selectObject( obj, true )
            }
        }
    } );
    
}

/*-----------Miscellaneous----------*/
function rotateRootObject( axis, invert )
{
	if( storedOptions == null )
		return;
	
    let v;
    let r;
    switch( axis || storedOptions.rootObjectRotation.axis ){
        case "X": 
            v = new THREE.Vector3( 0, 0, 1 );
            r = Math.PI / 2;
            break;
        case "Y":
            v = new THREE.Vector3( 1, 0, 0 ); 
            r = 0;
            break;
        case "Z": 
            v = new THREE.Vector3( 1, 0, 0);
            r = Math.PI / -2;
            break;
    }
    if( invert || storedOptions.rootObjectRotation.inverted ) 
        r = r== 0 ? Math.PI : r * -1;

    rootObj3d.matrix = new THREE.Matrix4();
    rootObj3d.matrix.makeRotationAxis( v, r );
    rootObj3d.applyMatrix( new THREE.Matrix4() );
}



function addComment(){
    enableCanvasCtrl( false );

    var content = 
    "<textarea id='annotationTextarea' autofocus style='float:left; min-height:40px'>" +
    "</textarea>" +
    "<div style='float:left;'>" +
        "<button id='annotationTextareaCloseBtn' class='annotation-button'><img src='img/icon_cancel.png'></button><br/>" +
        "<button id='annotationTextareaCheckBtn' class='annotation-button'><img src='img/icon_check.png'></button>" +
    "</div>"
    ;

    let point = pickPoint( contextMenuMousePos );
    if( point == null )
        point = {x: ( contextMenuMousePos.x + 1 ) / 2, y: ( contextMenuMousePos.y - 1 ) / -2};

    var id = annotations.add( pickObject( contextMenuMousePos ), point, content, "input" );
    document.getElementById( "annotationTextareaCloseBtn" ).setAttribute( "onclick", "closeComment(" + id + ", false)" );
    document.getElementById( "annotationTextareaCheckBtn" ).setAttribute( "onclick", "closeComment(" + id + ", true)" );
}

function closeComment( id, add ){
    var comment = document.getElementById( "annotationTextarea" ).value;
    annotations.remove( id )
    let point = pickPoint( contextMenuMousePos );
    if( point == null )
        point = {x: ( contextMenuMousePos.x + 1 ) / 2, y: ( contextMenuMousePos.y - 1 ) / -2};

    if( add && comment != "" )annotations.add( pickObject( contextMenuMousePos ), point, comment, "comment" );

    enableCanvasCtrl( true );
}

function showContextMenu( show ){
    var menu = document.getElementById( "contextMenu" );

    if( show ){
        //Dynamisches Ein- / Ausblenden bestimmter Einträge
        var objectsHidden = false;
        rootObj3d.traverse( function( obj ){
            if( !obj.visible )
                objectsHidden = true;
        } );
        
        document.getElementById( "restoreHiddenCompsBtn" ).style.display = objectsHidden ? "" : "none";

        var edgeDrawingStyles = ["documentation", "wireframe", "shadedEdges"];
        if( edgeDrawingStyles.indexOf( view.drawingStyle ) != -1 ){
            document.getElementById( "contextSubmenu1" ).style.display = "";
            document.getElementById( "contextSubmenu1Btn" ).style.display = "";
        }else{
            document.getElementById( "contextSubmenu1" ).style.display = "none";
            document.getElementById( "contextSubmenu1Btn" ).style.display = "none";
        }

        //Anzeige
        menu.style.display = "block";

        var left = event.clientX;
        var top = event.clientY;

        contextMenuMousePos = getMouseCoords( event );

        if( window.innerWidth - left - menu.offsetWidth < 0 )
            left = window.innerWidth - menu.offsetWidth;
        if( window.innerHeight - top - menu.offsetHeight < 0 )
            top = window.innerHeight - menu.offsetHeight;

        menu.style.left = left + "px";
        menu.style.top = top + "px";

    }else{
        menu.style.display = "none";
        hideSubmenus();
    }
}

function showSubmenu( id, show ){
   var submenu = document.getElementById( id );

   if( show ){
        submenu.style.display = "block";

        var button = document.getElementById( id + "Btn" );
        var wrapper = document.getElementById( "contextMenu" );

        var top = button.offsetTop + wrapper.offsetTop;
        var left = wrapper.offsetLeft + wrapper.offsetWidth;

        if( left + submenu.offsetWidth > window.innerWidth )
            left = left - submenu.offsetWidth - wrapper.offsetWidth;

        if( top + submenu.offsetHeight > window.innerHeight )
            top = window.innerHeight - submenu.offsetHeight;

        submenu.style.top = top + "px";
        submenu.style.left = left + "px";

   }else{
       submenu.style.display = "none";
   }
}

function hideSubmenus(){
    var i = 1;
    var submenu;
    while( submenu = document.getElementById( "contextSubmenu" + i ) ){
        submenu.style.display = "none";
        i ++;
    }
}

function recalculateEdges(){
    enableCanvasCtrl( true ); 
    showEdges( true, true );
}

function updateThresholdInfo(){
    document.getElementById( "drawingModeThresholdInfo" ).innerHTML = "Winkel: " + document.getElementById( "drawingModeThresholdSlider" ).value + "°";
}

function showBoundingBox( obj, show ){
    if( show ){
        var bbox = new THREE.BoxHelper( obj, 0xffffff );
        bbox.name = "BoundingBox|" + obj.name; 

        scene.add( bbox );
    }else{
        var box = scene.getObjectByName( "BoundingBox|" + obj.name );
        scene.remove( box );
    }
}

function enableCanvasCtrl( bool ){
    if( controls != undefined ) controls.enabled = bool;
}

globalKeyDown = function( event ){
    event = event || document.event;
    switch( event.key ){
        case "Escape":
            table.hideInfoContainer();
            setActiveMode( "" );
            break;
        case "ArrowDown":
            table.highlightTableRowByPos( table.highlightedTableRow, false );
            table.highlightTableRowByPos( table.highlightedTableRow +1, true);
            break;
        case "ArrowUp":
            table.highlightTableRowByPos( table.highlightedTableRow, false );
            table.highlightTableRowByPos( table.highlightedTableRow -1, true );
            break;
        case "c":
            if( event.ctrlKey )
                screenshotToClipboard();
            break;
        case "Control":
            if( activeMode == "move" ){
                var bbSize = new THREE.Vector3();
                new THREE.Box3().setFromObject( transformControls.object ).getSize( bbSize );
                transformControls.setTranslationSnap( bbSize.length() / 5 );
            }
            break;
        case "PageDown":
            views.showNextView();
            break;
        case "PageUp":
            views.showPrevView();
            break;
    }
}

globalKeyUp = function( event ){
    event = event || document.event;
    switch( event.key ){
        case "Control":
            if( activeMode == "move" ){
                transformControls.setTranslationSnap( null );
            }
            break;
    }
}

window.addEventListener("keydown", globalKeyDown, false );
window.addEventListener("keyup", globalKeyUp, false );

function screenshotToClipboard(){
    requestAnimationFrame( render );
    renderer.render( scene, camera );
    var image = renderer.domElement.toDataURL( "image/png" );
    var img = new Image();
    img.src = image;
    var test = window.open("");
    test.document.write( img.outerHTML );
}


/*------From w3schools.com------*/
function includeHTML() {
    var z, i, elmnt, file, xhttp;
    /*loop through a collection of all HTML elements:*/
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("w3-include-html");
        if (file) {
        /*make an HTTP request using the attribute value as the file name:*/
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
            if (this.status == 200) {elmnt.innerHTML = this.responseText;}
            if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
            /*remove the attribute, and call this function once more:*/
            elmnt.removeAttribute("w3-include-html");
            includeHTML();
            }
        } 
        xhttp.open("GET", file, true);
        xhttp.send();
        /*exit the function:*/
        return;
        }
    }
}
