Annotations = function(){
        this.list = [];
}

Annotations.prototype.add = function( obj3d, position, content, type ){
    let id = 0;
    let taken = true;
    while( taken ){
        taken = false;
        for( i in this.list ){
            if( this.list[i].id == id ){
                taken = true;
                id ++;
                break;
            }
        }
    } 

    this.list.push( {id:id, obj3d:obj3d, position:position, content:content, type:type} );

    if( type == "Object3D"){
        scene.add( content );
    }else{    
        var div = document.createElement( "div" );
        div.setAttribute( "id", "annotation" + id );
        div.setAttribute( "class", "annotation" + ( type ? " annotation-" + type : "" ) );
        div.setAttribute( "onmouseover", "annotations.showCloseBtn( " + id + ", true )" );
        div.setAttribute( "onmouseout", "annotations.showCloseBtn( " + id + ", false )" );

        var textBox = document.createElement( "pre" );
        textBox.innerHTML = content;

        var button = document.createElement( "button" );
        button.setAttribute( "id", "annotationCloseBtn" + id );
        button.setAttribute( "class", "annotation-button annotation-close-button" );
        button.setAttribute( "onclick", "annotations.remove(" + id + ")" );
        button.innerHTML = "<img src='img/icon_cancel_small.png'></button>";

        div.appendChild( textBox );
        div.appendChild( button );
        document.getElementById( "annotations" ).appendChild( div );

        this.list[id].height = div.offsetHeight;
        this.list[id].width =div.offsetWidth;
    }

    this.update( id );
    return id;
}

Annotations.prototype.remove = function( id ){
    var i = -1;
    for( j in this.list )
        if( this.list[j].id == id )
            i = j;
    if( i == -1 ){
        console.error( "Annotations.remove was given an id that has not been added: \"" + id + "\"" );
        return;
    }

    parentObj = this.list[i].obj3d;
    if( parentObj && parentObj.type == "Line" )
        parentObj.parent.remove( parentObj );
    


    if( this.list[i].type == "Object3D" )
        scene.remove( this.list[i].content );
    else
        document.getElementById( "annotations" ).removeChild( document.getElementById( "annotation" + id ) );

    let hasParent = true;
    if( this.list[i].obj3d != null ){
        var parentUUID = this.list[i].obj3d.uuid
        hasParent = false;
    }
    
    this.list.splice( i, 1 );    

    if( hasParent ){
        for( j in this.list )
            if( this.list[j].content.uuid == parentUUID )
                this.remove( this.list[j].id );
    }
}

Annotations.prototype.removeAll = function(){
    var i = this.list.length;
    if( i == 0 ) return;
    while( i-- ){
        if( this.list[i] ) this.remove( this.list[i].id ); //Durch das Entfernen eines Elements können mehrere auf einmal gelöscht werden
    }
}

Annotations.prototype.update = function( id ){
    function updateSingle( annotation ){
        if( displayMode == "2d" ){
            document.getElementById( "annotations" ).style.display = "none";
        }else if( annotation.type != "Object3D" ){
            document.getElementById( "annotations" ).style.display = "block";
            
            var div = document.getElementById( "annotation" + annotation.id ); 
            var bRect = renderer.domElement.getBoundingClientRect();

            if( annotation.obj3d == null ){
                div.style.left = ( annotation.position.x * window.innerWidth ) + bRect.left + "px";
                div.style.top = ( annotation.position.y * window.innerHeight ) + bRect.top + "px";
            }else{
                let vector = annotation.position.clone();
                let canvas = renderer.domElement;

                vector.project(camera);
                
                vector.x = Math.round( ( 0.5 + vector.x / 2 )  * ( canvas.width / window.devicePixelRatio ) ) + bRect.left;
                vector.y = Math.round( ( 0.5 - vector.y / 2 ) * ( canvas.height / window.devicePixelRatio ) ) + bRect.top;

                div.style.top = vector.y + "px";
                div.style.left = vector.x + "px";

                if( vector.y + annotation.height > window.innerHeight || vector.x + annotation.width > window.innerWidth  ){
                    div.style.display = "none";
                }else{
                    div.style.display = "";
                }
            }
        }
    }

    if( id == null )
        for( i in this.list )
            updateSingle( this.list[i] );
    else
        updateSingle( this.list[id] );
}

Annotations.prototype.showCloseBtn = function( id, show ){
    var annotation = document.getElementById( "annotation" + id );
    if( annotation.getAttribute( "class" ).indexOf( "annotation-input" ) != -1 || annotation.getAttribute( "class" ).indexOf( "annotation-marker" ) != -1 )
        show = false;

    var closeBtn = document.getElementById( "annotationCloseBtn" + id )
    if( show ){
        closeBtn.style.display = "block";
    }else{
        closeBtn.style.display = "";
    }
}