Table = function(){
    this.highlightedTableRow = 0;
}
/*---------------------Generische Funktionen---------------------*/
Table.prototype.header = function( arr ){
    let th = "<thead><tr>";
    for( i in arr ){
        th += "<th "+ ( arr.length == 1 ? "colspan=2" : "" ) +">" + arr[i] + "</th>";
    } 
    th += "</th></thead>";
    return th;
}
Table.prototype.table = function( obj, propKey, propValue )
{
    let table = "";
    for( property in obj )
	{
		var key = propKey   ? obj[ property ][ propKey ]   : property;
		var val = propValue ? obj[ property ][ propValue ] : obj[ property ];
		
		if( val == "" ) // Do not display rows with empty values
			continue;
		
		if( key == "transform" )
		{
			var arr = val;
			for( i in arr )
				if( arr[ i ] < 0.000000001 )
					arr[ i ] = 0;					
            
			table += "<tr>" + "<td>" + "Position" + "</td>" + "<td>" + "[ " + arr[ 12 ] + ", " + arr[ 13 ] + ", " + arr[ 14 ] + " ]" + "</td>" + "</tr>";
			
			table += "<tr>" + "<td>" + "Rotation" + "</td>" + "<td>" + 
				"[ " + arr[ 0 ] + ", " + arr[ 4 ] + ", " + arr[ 8 ]  + " ]" +
				"[ " + arr[ 1 ] + ", " + arr[ 5 ] + ", " + arr[ 9 ]  + " ]" +
				"[ " + arr[ 2 ] + ", " + arr[ 6 ] + ", " + arr[ 10 ] + " ]" + "</td>" + "</tr>";
							
			continue;
		}
		
		if( val && typeof( val ) != "string" )
			val = val.toString();
			
		if( val && val.substring( 0, 7 ) == "[object" )
			continue;
		
        table += "<tr>" + "<td>" + key + "</td>" + "<td>" + val + "</td>" + "</tr>";
    }
    return table;
}
Table.prototype.tableRow = function( arr, modifier ){
    let tr = "<tr " + ( modifier == null ? "" : modifier ) + ">";
    for( i in arr ){
        tr += "<td>" + arr[i] + "</td>";
    }
    tr += "</tr>";
    return tr;
}

Table.prototype.objSize = function( obj ){
    var bbox =  new THREE.Box3().setFromObject( obj );
    return  ( Math.abs( bbox.max.x - bbox.min.x ) * 1000 ).toFixed(1) + " x " + 
            ( Math.abs( bbox.max.y - bbox.min.y ) * 1000 ).toFixed(1) + " x " + 
            ( Math.abs( bbox.max.z - bbox.min.z ) * 1000 ).toFixed(1) + " [mm]"
}

Table.prototype.showInfoContainer = function( content, wide ){
    var objInfoContainer = document.getElementById( "objInfoContainer" );
    var objInfoTableContainer = document.getElementById( "objInfoTableContainer" );

    if( wide == true && window.innerWidth >= 800 ){
        objInfoContainer.style.width = "auto";
    }else{
       objInfoContainer.removeAttribute( "style" );
    }

    objInfoContainer.style.display = "inline";
    document.getElementById( "objInfo" ).innerHTML = content;
    document.getElementById( "objInfoHideBtn" ).style.display = "inline"; 
    
    objInfoTableContainer.style.height = ( document.body.clientHeight - objInfoContainer.offsetTop - 50 ) + "px";

    this.adjustModelPosition();
}

Table.prototype.hideInfoContainer = function(){
    document.getElementById( "objInfoContainer" ).style.display = "none";
    document.getElementById( "objInfo" ).innerHTML = "";
    document.getElementById( "objInfoHideBtn" ).style.display = "none";
    enableCanvasCtrl( true );
    this.adjustModelPosition();

    highlightedTableRow = null;
}


/*-----------------Spezifische Funktionen--------------------*/
//"Großer Tooltip"
Table.prototype.showObjInfo = function( obj )
{				
    if( obj == null || !tooltipEnabled )
        return;
    let content = "";	
    let objData = this.getObjData( obj );
    
    for( header in objData ){
        content += this.header( [header] );
        content += this.table( objData[header] );
    }
	
    this.showInfoContainer( content );
}

Table.prototype.showStatistics = function()
{
    var rootCadHandle = ( db.metadata.filename + "@" + db.metadata.configuration ).toLowerCase()
    
    var content = "";
    var masterData = db.drawings ? db.drawings[ rootCadHandle ] : null;
	
    content += this.header( [ "Dokument" ] );
    content += this.table( db.metadata );
    
	if( db.metadata.environment )
	{
		content += this.header( [ "Umgebung" ] );
		content += this.table( db.metadata.environment );
    }
	
	if( masterData && masterData.drawing )
	{
		content += this.header( [ "Zeichnung" ] );
		content += this.table( masterData.drawing );
    }
	
	if( masterData && masterData.part && masterData.part.part )
	{
		content += this.header( [ "Teil" ] );
		content += this.table( masterData.part );
    }
	
    content += this.header( [ "Statistik" ] );
    content += this.tableRow( ["Abmessungen", this.objSize( scene )] );
    content += this.tableRow( [ "Dokumente", Object.keys( db.docs ).length ] );
    
    var asmCount  = 0;
    var partCount = 0;
	
    for( doc in db.docs )
	{
		var docType = db.docs[ doc ].pmi.documenttype;
        if( docType == "part" )
            partCount ++;
        else if( docType == "assembly" )
            asmCount ++;
    }

    content += this.tableRow( [ "Baugruppen", asmCount ] );
    content += this.tableRow( [ "Einzelteile", partCount ] );
    
    var totalParts = 0;
    this.traverseStructure( db.docs[ rootCadHandle ].nameLower, function( docpath )
	{ 
        let docname = docpath.substring( docpath.lastIndexOf( "|" ) +1 );
        if( docname == "" )
            docname == docpath;

        if( db.docs[docname].pmi.documenttype == "part" )
            totalParts ++; 
    } );

    content += this.tableRow( [ "Einzelteile insgesamt", totalParts ] );

    var selectedObjects = [];
    rootObj3d.traverse( function( obj ){
        if( obj.type == "Object3D" && obj.userData.isHighlighted )
            selectedObjects.push( obj );
    } );

    if( Object.keys( selectedObjects ).length > 0 ){
        content += this.header( [ "Ausgewählte Dokumente" ] );

        for( i in selectedObjects ){
            var docpath = selectedObjects[i].name;
            var docname = docpath.substring( docpath.lastIndexOf( "|" ) + 1 );
            docname = this.nameIdToGeneric( docname );

            content += this.tableRow( [ db.drawings[docname].drawing.description1, docname ] );
        }
    }
    
    this.showInfoContainer( content );
}

Table.prototype.showProductStructure = function(){

    let headers = [];
    headers.push( "" );
    headers.push( "Sichtbar" );

    for( prop in storedOptions.productStructure )
        if( storedOptions.productStructure[prop] )
            headers.push( prop )

    let content = this.header( headers );
    
    let pos = 1, addedDocs = [];
    let rootDoc = ( db.metadata.filename + "@" + db.metadata.configuration ).toLowerCase()

    this.traverseStructure( "|" + db.docs[rootDoc].nameLower, function( docpath, level, quantity ){
        let docname = docpath.substring( docpath.lastIndexOf( "|" ) + 1 );
        doc = db.docs[docname];

        let hint = "";
        if( doc == null ){
            hint += "Fehlendes referenziertes Element"
        }if( db.protos[doc.nameLower] == null && doc.pmi.documenttype == "part" ){
            hint += ( hint == "" ? "" : ", " ) + "Keine Geometrie vorhanden"
        }
        
        if( addedDocs.indexOf( docpath ) == -1 ){
            addedDocs.push( docpath ); 

            let drawing = db.drawings[doc.nameLower].drawing;
            let part = db.drawings[doc.nameLower].part;

            let levelStr = "";
            let cbString = "";
            for( i = 0; i < level; i++ ){
                levelStr += ".  ";
                cbString += "__"
            }
            levelStr += level;

            

            let arrow = "";
            if( doc.refs.length > 0 ) {
                arrow = "<button class='toolbarBtn' onclick='table.trOnArrowClick( this, \"" + docpath + "\" )'> <img src='img/icon_arrow_down.png'></button>";
            }
            let visible = true;
            let doBreak = false;
            rootObj3d.traverse( function( obj ){
                if( !doBreak && table.nameIdToGeneric( obj.name ) == docpath && obj.visible === false ){
                    visible = false;
                    doBreak = true;
                }
            } );

            let checkBox = cbString + "<input type='checkbox' id='psCb" + docpath + "' onclick='table.trOnCbToggle( this, \"" + docpath + "\" )' " + ( visible ? "checked" : "" ) +  ">" 

            var map = {
                "": arrow,
                "Sichtbar": checkBox,
                "Position": pos,
                "Strukturtiefe": levelStr,
                "Anzahl": quantity,
                "Zustand": drawing.status,
                "Zeichnung": drawing.drawing,
                "Indexnummer": drawing.index,
                "CAD-Nummer": drawing.filename,
                "Konfiguration": drawing.configuration,
                "Teil": part.part,
                "Bezeichnung": drawing.description1,
                "Bezeichnung2": drawing.description2,
                "Hinweis": hint
            };

            var column = [];
            for( i in headers )
                column.push( map[headers[i]] );

            //docpath = "root" + docpath;
            content += table.tableRow( column, "id = \"" + docpath + "\" onclick='table.trOnMouseClick(" + pos + ", false)' pos=" + pos + " collapsed=false" );

            pos++;
        }
    }, 0, 1 )

    this.showInfoContainer( content, wide=true );
}

Table.prototype.trOnCbToggle = function( cb, docpath ){
    rootObj3d.traverse( function( obj ){
        let objName = table.nameIdToGeneric( obj.name );
        if( obj.type == "Object3D" && objName == docpath ){
            hideObject( obj, !cb.checked );

            if( cb.checked ){
                let parentNames = [];

                obj.traverseAncestors( function( parent ){
                    parent.visible = true;
                    parentNames.push( table.nameIdToGeneric( parent.name ) );
                } );

                for( i in parentNames ){
                    let name = parentNames[i];
                    let cb = document.getElementById( "psCb" + name );
                    if( cb ){
                        cb.checked = true;
                    }
                }
            }
        }
    } );
}

Table.prototype.trOnArrowClick = function( btn, docpath ){
    let rows = document.getElementById( "objInfo" ).children[1].children;

    let collapse;
    for( let i = 0; i < rows.length; i++ ){
        let row = rows[i];

        if( row.id == docpath ){
            collapse = !row.collapsed;
            row.collapsed = !row.collapsed;
            break;
        }
    }

    btn.children[0].src = collapse ? "img/icon_arrow_right.png" : "img/icon_arrow_down.png";

    for( let i = 0; i < rows.length; i++ ){
        let row = rows[i];
        if( row.id.indexOf( docpath ) == 0 && row.id != docpath ){
            row.style.display = collapse ? "none" : "";
        }
        
    }

}

Table.prototype.trOnMouseClick = function( pos, ignoreStructure ){
    selectNone();
    let trs = document.getElementById( "objInfo" ).children[1].children; //table rows
    for( i in trs ){
        table.highlightTableRow( trs[i].id, false );
    }

    this.highlightTableRowByPos( pos, true, ignoreStructure );
}

Table.prototype.highlightTableRowByPos = function( pos, highlight, ignoreStructure ){
    let t = document.querySelector( "#objInfo" );
    let tr =  t.querySelectorAll( "tr[pos='" + pos + "']" )[0];

    if( tr == null )
        return;

    selectNone();

    let trId = tr.attributes.id.value;
    this.highlightTableRow( trId, highlight );

    rootObj3d.traverse( function( obj ){
        let objPath = table.nameIdToGeneric( obj.name );
        if( ignoreStructure )
            objPath = objPath.substring( objPath.lastIndexOf( "|" ) );

        if( obj.type == "Object3D" && objPath == trId )
            selectObject( obj, true );
    });
}

Table.prototype.highlightTableRow = function( id, highlight ){
    let tr = document.getElementById( id );
    if( tr ){
        if( highlight ){
            tr.style.backgroundColor = "#ced8e0";
            tr.style.border = "2px dotted";
            tr.scrollIntoView( { behavior:"smooth", block:"nearest", inline:"nearest" } ); //TODO funktioniert nicht einwandfrei im IE 
            highlightedTableRow = parseInt( tr.attributes.pos.value );
        }else{
            tr.style.backgroundColor = "";
            tr.style.border = "";
        }
    }
}

Table.prototype.nameIdToGeneric = function( origName ){
    let objName = origName;
    let startPos;
    while( objName.indexOf(":") != -1 ){
        startPos = objName.indexOf( ":" );
        endPos = objName.indexOf( "|", startPos );
        if( endPos == -1 ) endPos = objName.length;
        objName = objName.substring( 0, startPos ) + objName.substring( endPos );
    }
    return objName;
}

Table.prototype.showAbout = function()
{
    var content = "";
    content += this.header([ xlt( "infoproalphaplm" )]);
    content += this.tableRow([ xlt( "productname" ),    productinformation[ "productname" ]]);
    content += this.tableRow([ xlt( "vendor" ),         productinformation[ "vendor" ]]);
    content += this.tableRow([ xlt( "revisionnumber" ), productinformation[ "revisionnumber" ]]);
    content += this.tableRow([ xlt( "builddate" ),      productinformation[ "builddate" ]]);    
    this.showInfoContainer( content );
}

Table.prototype.getObjData = function( obj ){
    let data = {};

    let objName           = obj.name.substring( obj.name.lastIndexOf( "|" ) + 1 );
    objName               = objName.substring( 0, objName.lastIndexOf(":") );     
	let masterDataDrawing = db.drawings[ objName ];
	let doc               = db.docs[ objName ];
				
	
	if( masterDataDrawing.drawing )
        data.Zeichnung = masterDataDrawing.drawing;
	
    if( masterDataDrawing.part )
        data.Teil = masterDataDrawing.part;
	
    if( doc.pmi ){
        data.Dokument = doc.pmi;
        data.Dokument.Abmessungen = this.objSize( obj );
	}
	
	if( masterDataDrawing.attributes )
        data["Merkmale Teil"] = masterDataDrawing.attributes;	
		
	if( obj.userData.attributes )
	{
        data.Instanz = obj.userData.attributes;
        for( key in obj.userData.attributes.attributes ){
            data.Instanz[key] = obj.userData.attributes.attributes[key];
        }
        for( key in obj.userData.attributes.material ){
            data.Instanz[key] = obj.userData.attributes.material[key];
        }
	}
		
	if( doc.pmi.configurations[ 0 ].properties )
	{
        data.Dateieigenschaften = {};
        let props = doc.pmi.configurations[0].properties;
        for( i in props ){
            data.Dateieigenschaften[props[i].prop_key] = props[i].prop_val;
        }
    } 

    return data;
}

Table.prototype.traverseStructure = function( docpath, callback, level, quantity )
{
    callback( docpath, level, quantity );

    let docname = docpath.substring( docpath.lastIndexOf( "|" ) + 1 );
    if( docname == "" )
        docname = docpath;

    level++;

    let refs = db.docs[ docname ].refs;
    for( i in refs )
	{
        var childDoc = db.docs[ refs[ i ].name ];
		if( childDoc )
			this.traverseStructure( docpath + "|" + childDoc.nameLower, callback, level, refs[ i ].quantity );
    }
}

Table.prototype.adjustModelPosition = function(){
    var smallWindow = ( window.innerWidth <= 800 ) ? true : false;
    var newWidth, newHeight;
    
    if( smallWindow ){
        newWidth = window.innerWidth;
        newHeight = window.innerHeight - document.getElementById( "objInfoContainer" ).offsetHeight;
    }else{
        newWidth = window.innerWidth - document.getElementById( "objInfoContainer" ).offsetWidth;
        newHeight = window.innerHeight;
    }
    
    renderer.setSize( newWidth, newHeight );

    var aspect = newWidth / newHeight;
    var frustrumSize = objSize / 2;
    
    cameraOrthographic.left = - frustrumSize * aspect;
    cameraOrthographic.right = frustrumSize * aspect;
    cameraOrthographic.top = frustrumSize;
    cameraOrthographic.bottom = - frustrumSize;
    cameraOrthographic.near = 0;
    cameraOrthographic.far = objSize * 5;

    camera.aspect = aspect;
    camera.updateProjectionMatrix();

    requestAnimationFrame( render );
}