Measurements = function(){
    this.measurePairs = [];
    this.measurePair = null;
    this.measureObjNr = 0;
    this.showPreviewPlane = false;
}

Measurements.prototype.setMeasureObjNr = function( nr ){
    this.measureObjNr = nr;
}

Measurements.prototype.getMeasureObjNr = function(){
    return this.measureObjNr;
}

Measurements.prototype.setMeasurePair = function(arg1, content ){
    if( Array.isArray( arg1 ) ){
        var pair = arg1;
        this.measurePair = pair;
    }else{
        var n = arg1;
        this.measurePair[n] = content;
    }
}

Measurements.prototype.getMeasurePair = function(){
    return this.measurePair;
}

Measurements.prototype.measure = function( mouse ){
    if( pickObject( mouse ) == null ){
        return null;
    }

    //Messpaar erstellen, wenn die Messung aus 2 Teilen besteht
    if( activeMode != "measureThickness" ){

        var measurePair = this.measurePairs[this.measurePairs.length -1] || this.measurePairs[this.measurePairs.length];
        if( measurePair == null || measurePair.length == 2 ){
            this.setMeasureObjNr( 0 );
            measurePair = this.measurePairs[this.measurePairs.length] = [];
        }else
            this.setMeasureObjNr( 1 );
        this.setMeasurePair( measurePair );
    } 

    switch( activeMode ){
        case "measureThickness":
            this.measureThickness( pickObject( mouse ), pickPoint( mouse ), pickFace( mouse ) );
            break;
        case "measurePointToPoint":
            this.measurePointToPoint( pickObject( mouse ), pickPoint( mouse ) );
            break;
        case "measurePlaneToPoint":
            this.measurePlaneToPoint( pickObject( mouse ), pickPoint( mouse ), pickFace( mouse ) );
            break;
        case "measureAngle":
            this.measureAngle( pickObject( mouse ), pickPoint( mouse ), pickFace( mouse ) );
            break;
    }
}

Measurements.prototype.measureThickness = function( object, point, face ){
    var normal = face.normal.clone();
    var mesh = object.children[0];

    var direction;
    var scalarProduct = normal.dot( new THREE.Vector3().subVectors( point, camera.position ) );
    if( scalarProduct > 0 )
        direction = normal;
    else
        direction = normal.multiplyScalar( -1 );

    direction.applyMatrix3( new THREE.Matrix3().getNormalMatrix( mesh.matrixWorld ) ).normalize();

    mesh.material.side = THREE.DoubleSide;
    raycaster.set( point, direction );
    intersects = raycaster.intersectObjects( rootObj3d.children, true );

    if( intersects.length < 2 ){
        raycaster.set( point, direction.multiplyScalar( -1 ) );
        intersects = raycaster.intersectObjects( rootObj3d.children, true );
    }

    mesh.material.side = THREE.FrontSide;

    var intersect1;
    var intersect2;
    for( i in intersects ){
        if( intersects[i].object.type == "Mesh" ){
            if( intersect1 == null ){
                intersect1 = intersects[i];
            }else{
                intersect2 = intersects[i];
                break;
            }
        }
    }

    if( intersect1 != null && intersect2 != null ){
        var endPoint = new THREE.Vector3().addVectors( point, direction.multiplyScalar( objSize / - 5 ) );
        var line = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [ point, endPoint ] ), new THREE.LineBasicMaterial({color:0x000000}) );
        annotations.add( mesh.parent, point, line, "Object3D" );

        var d = ( intersect1.point.distanceTo( intersect2.point ) * 1000 ).toFixed(1);
        annotations.add( line, endPoint, "T = " + d + " mm" );
    }
}

Measurements.prototype.measurePointToPoint = function( object, point ){
    this.setMeasurePair( this.getMeasureObjNr(), point );
    var measurePair = this.getMeasurePair();

    if( this.getMeasureObjNr() == 0 ){
        measurePair[0].id = annotations.add( object, point, "", "marker" )
        showModeDisplay( true, "Punkt-zu-Punkt-Distanz messen: Bitte zweiten Punkt wählen" );
    }else{
        var line = new THREE.Line( new THREE.BufferGeometry().setFromPoints( measurePair ), new THREE.LineBasicMaterial({color:0x000000}) );
        annotations.add( object, point, line, "Object3D" );

        var halfWayPoint = new THREE.Vector3().addVectors( this.measurePair[0], measurePair[1] ).divideScalar( 2 );

        let d = ( measurePair[0].distanceTo( measurePair[1] ) * 1000 ).toFixed(1);

        annotations.remove( measurePair[0].id );
        annotations.add( line, halfWayPoint, "d = " + d + " mm", "distance" );    
        
        for( var i = 0; i < 3; i++  ){
            var axis;
            var lineColor;
            switch( i ){
                case 0: axis = "x"; lineColor=0xff0000; break;
                case 1: axis = "y"; lineColor=0x00ff00; break;
                case 2: axis = "z"; lineColor=0x0000ff; break;
            }

            
            var startPoint;
            var endPoint;

            switch( axis ){
                case "x": startPoint = measurePair[0]; 
                            endPoint = new THREE.Vector3( measurePair[1].x, measurePair[0].y, measurePair[0].z );
                            break;
                case "y": startPoint = new THREE.Vector3( measurePair[1].x, measurePair[0].y, measurePair[0].z ); 
                            endPoint = new THREE.Vector3( measurePair[1].x, measurePair[1].y, measurePair[0].z );
                            break;
                case "z": startPoint = new THREE.Vector3( measurePair[1].x, measurePair[1].y, measurePair[0].z ); 
                            endPoint = new THREE.Vector3( measurePair[1].x, measurePair[1].y, measurePair[1].z );
                            break;

            }

            var lineA = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [ startPoint, endPoint ] ), new THREE.LineBasicMaterial({color:lineColor}) );
            annotations.add( object, startPoint, lineA, "Object3D" );

            var halfWayPointAxis = new THREE.Vector3().addVectors( startPoint, endPoint ).divideScalar( 2 );

            var da = ( Math.abs( measurePair[0][axis] - measurePair[1][axis] ) * 1000 ).toFixed(1); //distance axis
            annotations.add( lineA, halfWayPointAxis, "d" + axis + " = " + da + " mm" );
        }
        
        showModeDisplay( true, "Punkt-zu-Punkt-Distanz messen: Bitte ersten Punkt wählen" );
    }
}

Measurements.prototype.measurePlaneToPoint = function( object, point, face ){
    this.setMeasurePair( this.getMeasureObjNr(), point );
    var measurePair = this.getMeasurePair();

    if( this.getMeasureObjNr() == 0 ){
        var plane = this.placePlaneOnObject( object, point, face );
        measurePair[0].plane = plane.plane;
        measurePair[0].planeHelper = plane.planeHelper;

        showModeDisplay( true, "Ebene-zu-Punkt-Distanz messen: Bitte Punkt wählen" );
        this.showPreviewPlane = false;
    }else{
        measurePair[1] = point;
        var p1 = measurePair[0];
        var p2 = new THREE.Vector3(); measurePair[0].plane.projectPoint( measurePair[1], p2 );
        var p3 = measurePair[1];

        var line1 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [ p1, p2 ] ), new THREE.LineBasicMaterial({color:0xff0000}) );
        annotations.add( object, p1, line1, "Object3D" );
        annotations.add( line1, new THREE.Vector3().addVectors( p1, p2 ).divideScalar( 2 ), "dproj = " + ( p1.distanceTo( p2 ) * 1000 ).toFixed(1) + " mm" );

        var line2 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [ p2, p3 ] ), new THREE.LineBasicMaterial({color:0x000000}) );
        annotations.add( object, p2, line2, "Object3D" );
        annotations.add( line2, new THREE.Vector3().addVectors( p2, p3 ).divideScalar( 2 ), "d = " + ( p2.distanceTo( p3 ) * 1000 ).toFixed(1) + " mm" );

        annotations.remove( measurePair[0].planeHelper );
        showModeDisplay( true, "Ebene-zu-Punkt-Distanz messen: Bitte Ebene wählen" );
        this.showPreviewPlane = true;
    }
}

Measurements.prototype.measureAngle = function( object, point, face ){
    this.setMeasurePair( this.getMeasureObjNr(), point );
    var measurePair = this.getMeasurePair();

    if( this.getMeasureObjNr() == 0 ){
        var plane = this.placePlaneOnObject( object, point, face );
        measurePair[0].plane = plane.plane;
        measurePair[0].planeHelper = plane.planeHelper;
        showModeDisplay( true, "Winkel messen: Bitte zweite Fläche wählen" );
    }else{
        var plane = this.placePlaneOnObject( object, point, face );                
        measurePair[1].plane = plane.plane;
        measurePair[1].planeHelper = plane.planeHelper;

        annotations.remove( measurePair[1].planeHelper );

        var epsilon = measurePair[0].plane.normal.dot( measurePair[1].plane.normal );
        if( Math.abs( epsilon ) > 0.999 ){
            //Ebenen sind parallel zueinander
            measurePair.splice( 1, 1 );
            return null;
        }

        annotations.remove( measurePair[0].planeHelper );   

        var angle = 180 - THREE.Math.radToDeg( measurePair[0].plane.normal.angleTo( measurePair[1].plane.normal ) );

        //Schnittlinie berechnen
        var returnValues = this.intersectPlanes( measurePair[0].plane, measurePair[1].plane );   
        var iP = returnValues.point;
        var iDir = returnValues.direction;        
        
        //Projektionen der ausgewählten Punkte auf die Schnittlinie, um den Mittelpunkt des Kreises zu bestimmen
        var l1 = new THREE.Vector3().subVectors( measurePair[0], iP ).dot( iDir );
        var q1 = new THREE.Vector3().addVectors( iP, iDir.clone().multiplyScalar( l1 ) );

        var l2 = new THREE.Vector3().subVectors( measurePair[1], iP ).dot( iDir );
        var q2 = new THREE.Vector3().addVectors( iP, iDir.clone().multiplyScalar( l2 ) );

        var centerPoint = new THREE.Vector3().addVectors( q1, q2 ).divideScalar( 2 );

        var d1 = centerPoint.distanceTo( measurePair[0] );
        var d2 = centerPoint.distanceTo( measurePair[1] );
        var d = d1 < d2 ? d1 : d2;

        // Erzeugen der Achsen, mit denen der Kreis aufgebaut wird
        var lineSize = d * 0.8;
        var line1End = new THREE.Vector3().crossVectors( measurePair[0].plane.normal, iDir ).normalize().multiplyScalar( lineSize );
        var circleAxis1 = line1End.clone();
        var circleAxis2 = new THREE.Vector3().crossVectors( circleAxis1, iDir ).normalize().multiplyScalar( lineSize );

        var proj = new THREE.Vector3().subVectors( measurePair[0], measurePair[1] ).dot( measurePair[0].plane.normal );  
        if( proj < 0 ){
            circleAxis1.multiplyScalar( -1 );
            circleAxis2.multiplyScalar( -1 );
        }    

        var angles = [
            0,
            angle,
            360
        ]; 
        for( var j = 0; j < 2; j++ ){
            var vertices = [];
            var divisions = 40;
            for ( var i = 0; i <= divisions; i ++ ) {
                var v = ( ( i / divisions ) * THREE.Math.degToRad( angles[ j+1 ] - angles[ j ] ) ) + THREE.Math.degToRad( angles[ j ] );
                var x = Math.sin( v );
                var y = Math.cos( v );
                
                vertices.push( new THREE.Vector3().addVectors( circleAxis1.clone().multiplyScalar( y ), circleAxis2.clone().multiplyScalar( x ) ).add( centerPoint ) );	
                if( i == divisions / 2 )
                    annotationPoint = vertices[i];			
            }
            vertices.push( centerPoint );
            vertices.push( vertices[0] );

            for( var k = 0; k < 2; k++ ){
                var distanceFactorThreshold = 1;
                var circleAxis = k == 0 ? circleAxis1.clone() : circleAxis1.clone().applyAxisAngle( iDir, - THREE.Math.degToRad( angle ) );

                var distanceFactor = centerPoint.distanceTo( measurePair[k] ) / circleAxis.length();
                if( distanceFactor > distanceFactorThreshold ){
                    vertices.push( centerPoint );
                    vertices.push( circleAxis.normalize().multiplyScalar( centerPoint.distanceTo( measurePair[k] ) ).add( centerPoint ) );
                    vertices.push( centerPoint );
                }
            }

            var circleSegment = new THREE.Line( new THREE.BufferGeometry().setFromPoints( vertices ), new THREE.LineBasicMaterial({color:0x000000}) );
            annotations.add( object, vertices[1], circleSegment, "Object3D" );
            annotations.add( circleSegment, annotationPoint, Math.round( angles[j+1] - angles[j] ) + "°" ); 
        }
        showModeDisplay( true, "Winkel messen: Bitte erste Fläche wählen" );
    }
}

Measurements.prototype.intersectPlanes = function(p1, p2) {
    //Algorithm taken from http://geomalgorithms.com/a05-_intersect-1.html
    var direction = new THREE.Vector3().crossVectors(p1.normal, p2.normal).normalize()

    var magnitude = direction.distanceTo(new THREE.Vector3(0, 0, 0))
    if (magnitude === 0) {
        return null
    }
  
    var X = Math.abs(direction.x)
    var Y = Math.abs(direction.y)
    var Z = Math.abs(direction.z)
  
    var point
  
    if (Z >= X && Z >= Y) {
        point = this.solveIntersectingPoint('z', 'x', 'y', p1, p2)
    } else if (Y >= Z && Y >= X){
        point = this.solveIntersectingPoint('y', 'z', 'x', p1, p2)
    } else {
        point = this.solveIntersectingPoint('x', 'y', 'z', p1, p2)
    }
  
    return {point:point, direction:direction}
}

Measurements.prototype.solveIntersectingPoint = function(zeroCoord, A, B, p1, p2){
    var a1 = p1.normal[A]
    var b1 = p1.normal[B]
    var d1 = p1.constant

    var a2 = p2.normal[A]
    var b2 = p2.normal[B]
    var d2 = p2.constant

    var A0 = ((b2 * d1) - (b1 * d2)) / ((a1 * b2 - a2 * b1))
    var B0 = ((a1 * d2) - (a2 * d1)) / ((a1 * b2 - a2 * b1))

    var point = new THREE.Vector3()
    point[zeroCoord] = 0
    point[A] = A0
    point[B] = B0

    point.multiplyScalar( -1 );

    return point
}

function getIntersectionLines( mesh, plane ) 
{
	var geometry = new THREE.Geometry();
	geometry.fromBufferGeometry( mesh.geometry );
	
	var pointsOfIntersection = new THREE.Geometry();
	var a = new THREE.Vector3();
	var	b = new THREE.Vector3();
	var	c = new THREE.Vector3();		
	var p = new THREE.Vector3();					
	var lineAB = new THREE.Line3( a, b );
	var lineBC = new THREE.Line3( b, c );
	var lineCA = new THREE.Line3( c, a );									
	var mat = new THREE.LineBasicMaterial({ color: 0xffff00 });
							
	geometry.faces.forEach( function( face ) 
	{									  		
		mesh.localToWorld( a.copy( geometry.vertices[ face.a ]));
		mesh.localToWorld( b.copy( geometry.vertices[ face.b ]));
		mesh.localToWorld( c.copy( geometry.vertices[ face.c ]));
		
		lineAB.set( a, b );
		lineBC.set( b, c );
		lineCA.set( c, a );
		
		if( plane.intersectLine( lineAB, p ))
			pointsOfIntersection.vertices.push( p.clone());
		
		if( plane.intersectLine( lineBC, p ))
			pointsOfIntersection.vertices.push( p.clone());
		
		if( plane.intersectLine( lineCA, p ))
			pointsOfIntersection.vertices.push( p.clone());
	});
		
	return new THREE.LineSegments( pointsOfIntersection, mat );					
}	

Measurements.prototype.placePlaneOnObject = function( object, point, face ){
    var normal = face.normal.clone();

    normal.applyMatrix3( new THREE.Matrix3().getNormalMatrix( object.matrixWorld ) ).normalize();

    var plane = new THREE.Plane().setFromNormalAndCoplanarPoint( normal, point );

    var bbSize = new THREE.Vector3();
    new THREE.Box3().setFromObject( object ).getSize( bbSize );
    var planeSize = bbSize.length() / 2;

    var planeMat =  new THREE.MeshLambertMaterial( { transparent: true, opacity: 0.2, color: 0x00ff00 } );
    planeMat.side = THREE.DoubleSide;
    var planeHelper = new THREE.Mesh( new THREE.PlaneGeometry( planeSize, planeSize, 1, 1 ), planeMat );

    planeHelper.position.set( point.x + ( normal.x * objSize / 5000 ), 
                              point.y + ( normal.y * objSize / 5000 ), 
                              point.z + ( normal.z * objSize / 5000 ) );
    planeHelper.lookAt( new THREE.Vector3().addVectors( point, normal ) );

    var p1 = new THREE.Vector3( planeSize / -2, planeSize / -2, 0 );
    var p2 = new THREE.Vector3( planeSize / -2, planeSize / 2, 0 );
    var p3 = new THREE.Vector3( planeSize / 2, planeSize / 2, 0 );
    var p4 = new THREE.Vector3( planeSize / 2, planeSize / -2, 0 );

    var line1 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [p1, p2] ), new THREE.LineBasicMaterial({color:0x000000}) );
    var line2 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [p2, p3] ), new THREE.LineBasicMaterial({color:0x000000}) );
    var line3 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [p3, p4] ), new THREE.LineBasicMaterial({color:0x000000}) );
    var line4 = new THREE.Line( new THREE.BufferGeometry().setFromPoints( [p4, p1] ), new THREE.LineBasicMaterial({color:0x000000}) );

    planeHelper.add( line1 );
    planeHelper.add( line2 );
    planeHelper.add( line3 );
    planeHelper.add( line4 );

    var planes = {};
    planes.planeHelper = annotations.add( object, point, planeHelper, "Object3D" );
    planes.plane = plane;
    return planes;
}