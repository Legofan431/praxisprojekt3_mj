Views = function(){
    this.list = [];
    this.curView = -1;
    this.isPlaying = false;
}

Views.prototype.getView = function()
{    
	var hiddenComps = [];
	var translated  = [];	
	
	rootObj3d.traverse( function( obj )
	{
        if( !obj.visible && obj.type == "Object3D" ) 
			hiddenComps.push( obj );
		
		if( obj.type == "Object3D" && obj.userData.positionOrig && !obj.userData.positionOrig.equals( obj.position ) )		
			translated[ obj.name ] = { 'object' : obj, 'position' : obj.position.clone()};
    });

    return {
        "camPos": camera.position.clone(),
        "camUp": camera.up.clone(),
        "camTarget": controls.target.clone(),
        "drawingStyle": view.drawingStyle,
        "isCameraPerspective": camera == cameraPerspective ? true : false,
        "annotations": annotations.list.slice(0), //Klont das Array
        "measurePairs": measurements.measurePairs.slice(0),
        "upAxis": storedOptions.rootObjectRotation.axis,
        "upAxisInverted": storedOptions.rootObjectRotation.inverted,
        "hiddenComps": hiddenComps,
        "name": this.list[this.curView] && this.list[ this.curView ].name || "Abb. " + ( this.list.length +1 ),
        "translated": translated
    };
}

Views.prototype.setView = function( nr )
{
    this.setViewName();
	
    if( nr == -1 ) 
		return;

    var params = this.list[ nr ];
    if( params == null )
	{
        console.error( "views.setView(): View number " + nr + " is out of range" );
        return;
    }

    if( params.isCameraPerspective != undefined)
        setPerspective( params.isCameraPerspective ? "cameraPerspective" : "cameraOrthographic" );
    if( params.camPos != undefined && params.camUp != undefined && params.camTarget != undefined )
        setCamPos( params.camPos, params.camUp, params.camTarget );
    if( params.drawingStyle != undefined )
        setDrawingStyle( params.drawingStyle ); //Ruft setMaterialStyle auf, wo die Opazität der ausgeblendeten Meshes manipuliert wird

    var compsToFade = [];
	var positions   = [];
	
    rootObj3d.traverse( function( obj )
	{
		if( obj.type == "Object3D" ){
            if( obj.userData.positionOrig )
			    positions[ obj.name ] = { 'object' : obj, 'position' : obj.userData.positionOrig };
				
            if( !obj.visible && params.hiddenComps.indexOf( obj ) == -1 )
                //Aktuell ausgeblendet, soll eingeblendet werden
                compsToFade.push( obj );
        }
    });
	requestRender();
	for( name in params.translated )
		positions[ name ].position = params.translated[ name ].position;
	
	for( name in positions )
	{
		var elem = positions[ name ];
        var obj  = elem.object;
        var pos  = elem.position;
		
        new TWEEN.Tween( obj.position )
        .to({ 
			x: pos.x, 
			y: pos.y, 
			z: pos.z }, storedOptions.animation.duration )
        .easing( TWEEN.Easing.Quadratic.InOut )
        .start();
    }
	
    for( i in params.hiddenComps ){
        if( compsToFade.indexOf( params.hiddenComps[i] ) == -1 && params.hiddenComps[i].visible )
            //Aktuell eingeblendet, soll ausgeblendet werden
            compsToFade.push( params.hiddenComps[i] );
    }
    
    for( i in compsToFade )
	{
        let obj3d = compsToFade[i];
        obj3d.traverse( function( comp ){
            if( comp.type == "Mesh" || comp.type == "LineSegments" )
            {                
                comp.material.transparent = true;
                renderer.sortObjects = true;

                let startOpacity;
                let endOpacity;

                comp.parent.visible = true;
                if( params.hiddenComps.indexOf( comp.parent ) != -1 )
                {
                    startOpacity = comp.material.opacity;
                    endOpacity = 0;
                }
                else
                {
                    startOpacity = 0;
                    endOpacity = comp.type == "Mesh" ? comp.material.opacity : 1;
                }

                comp.material.opacity = startOpacity;

                new TWEEN.Tween( comp.material )
                .to( { opacity: endOpacity }, params.isAssemblyAnimation ? storedOptions.animation.assembly : storedOptions.animation.duration )
                .easing( TWEEN.Easing.Quadratic.InOut )
                .onComplete( function(){
                        if( endOpacity == 0 ){
                            hideObject( obj3d, true );
                            comp.material.opacity = startOpacity;
                        }else{
                            hideObject( obj3d, false );
                        }
                        if( params.isAssemblyAnimation ){
                            let view = views.curView -1;
                            let newObj;
                            if( view == -1 ){
                                rootObj3d.traverse( function( obj ){
                                    let childrenOnlyMeshes = true;
                                    for( i in obj.children ){
                                        if( obj.children[i].type == "Object3D" ){
                                            childrenOnlyMeshes = false;
                                            break;
                                        }
                                    }
                                    if( obj.visible && childrenOnlyMeshes ){
                                        newObj = obj;
                                    }
                                } );
                            }else{
                                newObj = views.list[view].hiddenComps[views.list[view].hiddenComps.length -1];
                            }
                            selectNone();
                            selectObject( newObj, true );
                        }
                    } )
                .start();
            }    
        } ); 
    }
	         
    rotateRootObject( params.upAxis, params.upAxisInverted );

    annotations.removeAll();	
    for( i in params.annotations )
	{
        let a = params.annotations[i];
        annotations.add( a.obj3d, a.position, a.content, a.type );
    }

    //measure...    
}

Views.prototype.addView = function( params ){
    this.curView = this.list.length;
    var params = params || this.getView();
    this.list.push( params );
    this.setViewName();
}

Views.prototype.overwriteView = function(){
    this.list.splice( this.curView, 1, this.getView() );
}

Views.prototype.deleteView = function( viewId ){
    this.list.splice( viewId || this.curView, 1 )
    if( this.curView >= this.list.length )
        this.curView--
    this.setView( this.curView );
}

Views.prototype.showNextView = function(){
    if( this.list.length == 0)
        return;
    this.setView( this.curView < this.list.length - 1 ? ++this.curView : this.curView = 0 );

    //Autoplay
    function onEnd(){
        if( views.isPlaying )
            views.showNextView();
    }

    let duration;
    if( this.list[this.curView].isAssemblyAnimation )
        duration = storedOptions.animation.assembly;
    else
        duration = storedOptions.animation.duration;

    new TWEEN.Tween()
    .to({}, duration * 1.05 )
    .onComplete( onEnd )
    .start();
}

Views.prototype.showPrevView = function(){
     this.setView( this.curView > 0 ? --this.curView : this.curView = this.list.length -1 );
}

Views.prototype.setViewName = function(){
    var textField = document.getElementById( "viewNamer" );
    if( this.curView == -1 ){
        textField.value = ""
        textField.style.display = "none";
    }else{
        textField.value = this.list[ this.curView ].name;
        textField.style.display = "block";
    }
}

Views.prototype.handleViewNamerKeypress = function( event ){
    var textField = document.getElementById( "viewNamer" )
    if( event.key == "Enter" && this.curView != -1 ){
        for( i in this.list ){
            if( this.list[i].name == textField.value ){
                alert( "Der Name der Ansicht ist bereits vergeben." );
                textField.value = this.list[this.curView].name;
                return;
            }
        }
        this.list[this.curView].name = textField.value;
        textField.blur();
    }
}

Views.prototype.playPause = function(){
    if( this.isPlaying == false ){
        this.isPlaying = true;
        this.showNextView();
    }else{
        this.isPlaying = false;
    }
}

Views.prototype.setOrder = function( order ){
    let newOrder = [];
    for( i in order ){

        for( j in this.list ){
            if( this.list[j].name == order[i] ){
                newOrder.push( this.list[j] );
                break;
            }
        }
    }
    this.list = newOrder;
}

Views.prototype.generateInstructions = function(){
    for( let i = this.list.length -1; i >= 0; i-- ){
        let view = this.list[i];
        if( view.name.indexOf( "Schritt" ) == 0 ){
            this.deleteView( i )
        }
    }

    let rootDoc = ( db.metadata.filename + "@" + db.metadata.configuration ).toLowerCase();
    let knownDocPaths = {};
    let installedParts = [];
    let step = 1;

    let docs = [];
    table.traverseStructure( db.docs[rootDoc].nameLower, function( docPath ){
        if( knownDocPaths[docPath] == null)
            knownDocPaths[docPath] = 1;
        else
            knownDocPaths[docPath] ++;

        let docPathId = "";
        let docPathWithoutId = "";
        let docComps = docPath.split("|");
        for( i in docComps){
            docComp = docComps[i];

            docPathWithoutId += "|" + docComp; if( docPathWithoutId.indexOf( "|" ) == 0 ) docPathWithoutId = docPathWithoutId.substring( 1 );
            docPathId += "|" + docComp + ":" + knownDocPaths[docPathWithoutId];
        }
        if( rootObj3d.getObjectByName( docPathId ) )
            docs.push( docPathId );
    } );

    for( let i = docs.length -1; i > 0; i-- ){
        let docPathId = docs[i];

        let obj = rootObj3d.getObjectByName( docPathId );
        let objName = table.nameIdToGeneric( obj.name.substring( obj.name.lastIndexOf("|") +1 ) );

        if( db.docs[objName] && db.docs[objName].pmi.documenttype != "assembly" ){
            let params = {};
            params.hiddenComps = [];
            
            installedParts.push( obj.name );

            rootObj3d.traverse( function ( child ){
                let childrenOnlyMeshes = true;
                for( j in child.children ){
                    if( child.children[j].type == "Object3D"){
                        childrenOnlyMeshes = false;
                        break;
                    }
                }

                if( child != obj && child.type == "Object3D" && childrenOnlyMeshes && installedParts.indexOf( child.name ) == -1 ){
                    params.hiddenComps.push( child );
                }
            } );

            params.name = "Schritt " + step++;
            params.isAssemblyAnimation = true;
            views.addView( params );
        }
    }
}